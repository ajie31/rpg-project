﻿
using UnityEngine;
using TMPro;

public class swordSkill : MonoBehaviour
{
    [Header("Basic Player Attribute")]
    public playerStatusOBJ _pStatus;
    public healingSystem _heal;
    public statusEffect _statusEffect;
    public DamageIO _damageIO;
    
   // public playerReg _preg;
    [Header("Sword Skills")]
    public demonBlood _demonBlood;
    public trueStrike _trueStrike;
    public dimensionRift _dimensionRift;
    public soulRender _soulRender;
    public hellWrath _hellWrath;
    //skill demon blood
    private float durationExplotion;

    private float delayRiftDuration;

    private float soulRenderDuration;

    private float hellwrathDuration, hellwrathPerSec = 3;

    private Transform playerT;
    public playerFollow _pfollow;
    public playerCollider _pCollider;
    public combatScript _combat;
    public cameraManager _managerC;
    

    void Start()
    {
      
        playerT = GameObject.FindGameObjectWithTag("Player").transform;

        registerPlayer(playerT.GetComponent<playerFollow>(), playerT.GetComponent<playerCollider>(),
            playerT.GetComponent<combatScript>(), Camera.main.GetComponent<cameraManager>());

    }
    public void registerPlayer(playerFollow _pf, playerCollider _pcol, combatScript _com, cameraManager _cM)
    {
        _pfollow = _pf;
        _pCollider = _pcol;
        _combat = _com;
        _managerC = _cM;
    }
    // Update is called once per frame
    void Update()
    {
        if (durationExplotion > 0)
        {
            durationExplotion -= Time.deltaTime;
            if (durationExplotion <= _demonBlood.getDelay() - 1f && _pCollider._agent._State == State.skill)
            {

                _pCollider.setIsSkill(false);
               
            }
        }
        else if (durationExplotion<=0 && _demonBlood.isExplode)
        {

            _demonBlood.skillOver(_combat);
        }

        //dimension rift
        if (delayRiftDuration > 0)
        {
            delayRiftDuration -= Time.deltaTime;
        }
        else if (delayRiftDuration <= 0 && _dimensionRift.isDimensionRift)
        {
            // Debug.Log("blink");
            _dimensionRift.afterBlink(_pCollider,_combat,_pfollow);

            StartCoroutine(_managerC.teleporting());
            

            _pCollider.attack(false,false,false,true);
        }

        if (soulRenderDuration > 0)
        {
            soulRenderDuration -= Time.deltaTime;

            if (soulRenderDuration <= _soulRender.getDuration() - 1 && _pCollider._agent._State == State.skill)
            {

                _pCollider.setIsSkill(false);
              
            }
        }
        else if (soulRenderDuration <= 0 && _soulRender.isSoulRender)
        {
            _soulRender.skillOver(_combat);
        }

        if (hellwrathDuration > 0)
        {
            if (hellwrathDuration <= hellwrathPerSec)
            {
                //_combat.skillArea(true, _skill[0].areaRange[rangeExplotionUp1], _skill[4].damageInstant[damageInstantUp5]);
                _hellWrath.damageOut(_combat);
                hellwrathPerSec -= 1;
            }
            hellwrathDuration -= Time.deltaTime;
        }
        else if(hellwrathDuration <= 0 && _hellWrath.isHellWrath)
        {
            _hellWrath.skillOver(_pCollider);
           // _pCollider.setIsSkill(false);
        }
    }

    public void DemonBlood()
    {
        _demonBlood.castPower(_pCollider,_combat);
        durationExplotion = _demonBlood.getDelay();
    }

    //lack of silent debuff
    public void trueStrike()
    {
        _trueStrike.castPower(_pCollider,_combat);
    }
 
    public void dimensionRift()
    {
        _dimensionRift.castPower(_pCollider,_combat,_pfollow);
        delayRiftDuration = _dimensionRift.getDelayDuration();
    }

    public void soulRender()
    {
        _soulRender.castPower(_pCollider,_combat);
        soulRenderDuration = _soulRender.getDuration();
    }

    public void HellWrath()
    {

        _hellWrath.castPower(_pCollider);
        hellwrathDuration = _hellWrath.hellwrathDurationCount;
        hellwrathPerSec = _hellWrath.hellwrathPerSec;
    }

    private void OnEnable()
    {
        _demonBlood.registerSkillAction(DemonBlood);
       _trueStrike.registerSkillAction(trueStrike);
        _dimensionRift.registerSkillAction(dimensionRift);
        _soulRender.registerSkillAction(soulRender);
        _hellWrath.registerSkillAction(HellWrath);
    }
}
