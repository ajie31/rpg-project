﻿using TMPro;
using UnityEngine;
[CreateAssetMenu(fileName = "new enemy", menuName = "enemy/enemy Profile")]
public class enemyProfile : ScriptableObject
{
    [Header("Player General")]
    #region player prop
    public DamageIO _pDamageIO;
    public statusEffect _pStatusEffet;
    #endregion

    #region enemy general prop
    private enemyUIManager _ui;
    [HideInInspector] public Transform _enemyT;
    #endregion

    [Header("Enemy Attribute")]
    public playerAttribute[] enemyAttribute;

    [Header("Life Status")]
    public bool deaths;

    #region damageTaken process
    float damageProcess = 0;
    #endregion

    #region text Appear
    private TextMeshProUGUI _text;
    #endregion

    #region attack property
    public float rangeDetection;
    public float weaponRange;
    #endregion

    public delegate void enemyAttack();
    public enemyAttack attackCommand;

    #region Get general attribute

    public float getTotalAttributeValue(attributes attr)
    {
        foreach (var attrib in enemyAttribute)
        {
            if (attrib._attr == attr)
            {
                return attrib.totalValue;

            }
        }
        return 0;
    }

    public void resetAttribute(attributes attr)
    {
        foreach (var attrib in enemyAttribute)
        {
            if (attrib._attr == attr)
            {
                attrib.actualValue = attrib.totalValue;
                break;
            }
        }
    }
    #endregion



    public void enemyRegister(Transform t,enemyUIManager ui)
    {
        _enemyT = t;
        _ui = ui;
    }

}
