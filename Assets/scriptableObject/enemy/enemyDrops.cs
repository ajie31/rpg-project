﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new drop", menuName = "Items and Inventory/ItemDrop")]
public class enemyDrops : ScriptableObject
{
   
    public dropItem[] _itemDropProp;

    public int total;
   
    public void generateItem(GameObject item)
    {
        int randNum = Random.Range(0, total+1);
        foreach (var items in _itemDropProp)
        {
            if (randNum <= items.weight)
            {

                item.transform.SetParent(null);
                item.GetComponent<item>().setItemObj(items._iteomObj);
                item.SetActive(true);
                break;
            }
            else
            {
                randNum -= items.weight;
            }
            
        }
        
    }
}

[System.Serializable]
public class dropItem
{
   
    public itemsObject _iteomObj;
    [Range(0, 100)]
    public int weight;

}
