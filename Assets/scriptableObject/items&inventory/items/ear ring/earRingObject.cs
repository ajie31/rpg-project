﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Ear Ring object", menuName = "Items and Inventory/Items/Ear Ring")]
public class earRingObject : itemsObject
{
    public void Awake()
    {
        type = itemType.earRing;
    }
}
