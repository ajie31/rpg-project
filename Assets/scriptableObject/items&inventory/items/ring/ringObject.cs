﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Ring object", menuName = "Items and Inventory/Items/Ring")]
public class ringObject : itemsObject
{
    public void Awake()
    {
        type = itemType.ring;
    }
}
