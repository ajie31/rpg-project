﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "new potion object",menuName ="Items and Inventory/Items/Potions")]
public class potionObject : itemsObject
{

    public void Awake()
    {
        type = itemType.potions;
    }
}
