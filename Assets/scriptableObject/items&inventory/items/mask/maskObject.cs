﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Mask object", menuName = "Items and Inventory/Items/Mask")]
public class maskObject : itemsObject
{
    public void Awake()
    {
        type = itemType.mask;
    }
}
