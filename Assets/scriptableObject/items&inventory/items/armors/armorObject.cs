﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new armor object", menuName = "Items and Inventory/Items/armor")]
public class armorObject : itemsObject
{
    

    public void Awake()
    {
        type = itemType.weapon;
    }
}
