﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum itemType
{
    mask,
    earRing,
    glove,
    weapon,
    bodyArmor,  
    ring,    
    boot,
    potions 
}

public enum attributes
{
    Health,
    Armor,
    Attack,
    MovementSpd,
    AttackSpd,
    Evasion,
    ManaPoint,
    Crits,
    strength,
    agility,
    intelegent
}


public abstract class itemsObject : ScriptableObject
{
    public int itemId;
    public string itemName;
    public Sprite _sprite;
    public itemType type;
    public attributeBonus[] _attBonus;
    [TextArea(3, 8)]
    public string description;
    public int itemValue;
}

[System.Serializable]
public class attributeBonus :ISerializationCallbackReceiver
{
    public int bonusAtt;
    public attributes attribute;
    public string attributeName;
    public attributeBonus (int _bonus, attributes _attribute)
    {
        bonusAtt = _bonus;
        attribute = _attribute;
    }

    public void OnAfterDeserialize()
    {
        switch (attribute)
        {
            case attributes.Health:
                attributeName = "Health";
                break;
            case attributes.Armor:
                attributeName = "Armor";
                break;
            case attributes.Attack:
                attributeName = "Attack";
                break;
            case attributes.MovementSpd:
                attributeName = "Movement.S";
                break;
            case attributes.AttackSpd:
                attributeName = "Attack.S";
                break;
            case attributes.Evasion:
                attributeName = "Evasion";
                break;
            case attributes.ManaPoint:
                attributeName = "ManaPoint";
                break;
            case attributes.Crits:
                attributeName = "Crits%";
                break;
            case attributes.strength:
                attributeName = "Strength";
                break;
            case attributes.agility:
                attributeName = "Agility";
                break;
            case attributes.intelegent:
                attributeName = "Intelegent";
                break;
            default:
                attributeName = " ";
                break;
        }
    }

    public void OnBeforeSerialize()
    {
       
    }
}
