﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new database",menuName = "Items and Inventory / Item Database")]
public class itemDatabaseObj : ScriptableObject, ISerializationCallbackReceiver
{
    public itemsObject[] items;
    public Dictionary<itemsObject, int> getId = new Dictionary<itemsObject, int>();
    public Dictionary<int, itemsObject> getItem = new Dictionary<int, itemsObject>();

    public void OnAfterDeserialize()
    {
        getId = new Dictionary<itemsObject, int>();
        getItem = new Dictionary<int, itemsObject>();
        for (int i = 0; i < items.Length; i++)
        {
            getId.Add(items[i], i);
            getItem.Add(i, items[i]);
            items[i].itemId = i;
        }
    }

    public void OnBeforeSerialize()
    {
        for (int i = 0; i < items.Length; i++)
        {
            items[i].itemId = i;

        }
    }
}
