﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Weapon object", menuName = "Items and Inventory/Items/Weapon")]
public class weaponObject : itemsObject
{
    public weaponClass _weaponClass;
    public void Awake()
    {
        type = itemType.weapon;
    }
}
