﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Boot object", menuName = "Items and Inventory/Items/boot")]
public class bootsObject : itemsObject
{
    public void Awake()
    {
        type = itemType.boot;
    }
}
