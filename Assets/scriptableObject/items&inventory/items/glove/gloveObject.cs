﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Glove object", menuName = "Items and Inventory/Items/Glove")]
public class gloveObject : itemsObject
{
    public void Awake()
    {
        type = itemType.glove;
    }
}
