﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="new inventory",menuName = "Items and Inventory/Inventory system/Inventory")]
public class inventoryObject : ScriptableObject, ISerializationCallbackReceiver
{
    public itemDatabaseObj database;
    public List<inventorySlot> container = new List<inventorySlot>();
    public bool isEquipment;
    public int slotLimit;

    public void addItem(itemsObject _item, int _amount)
    {
        if (isEquipment)
        {
            if (container.Count < slotLimit)
            {
                container.Add(new inventorySlot(database.getId[_item], _item, _amount));
            
            }
            

            /**
             * make slot limit warning here
             * 
             * 
             * */

        }
        else
        {
            for (int i = 0; i < container.Count; i++)
            {
                if (container[i].item == _item)
                {
                    container[i].addAmount(_amount);
                   // playerUIManager._pUIManager.updateIsHeal(container[i].itemId, container[i].amount);
                    return;
                }
            }
            container.Add(new inventorySlot(database.getId[_item], _item, _amount));
           // playerUIManager._pUIManager.updateIsHeal(_item.itemId, _amount);
        }
               
    }

    public void OnBeforeSerialize()
    {
        for (int i = 0; i < container.Count; i++)
        {
            container[i].item = database.getItem[container[i].itemId];
        }
    }

    public void OnAfterDeserialize()
    {
       
    }

    public int getItemAmount(itemsObject _item)
    {
        for (int i = 0; i < container.Count; i++)
        {
            if (container[i].item == _item)
            {
                return container[i].amount;   
            }
        }
        return 0;
    }

    public void decreaseItem(int id,int amount)
    {             
        for (int i = 0; i < container.Count; i++)
        {
            if (database.getId[container[i].item] == id)
            {
                if (container[i].amount <= 1)
                {
                    container.RemoveAt(i);
                }
                else
                {
                    container[i].amount -= amount;
                    playerUIManager._pUIManager.updatePotion(container[i].item);
                    
                }
               
                break;
            }
        }         
    }
}


[System.Serializable]
public class inventorySlot
{
    public int itemId;
    public itemsObject item;
    public int amount;
    public bool isEquiped;
    public int indexEquip;
    public inventorySlot(int _id,itemsObject _item, int _amount,int _indexEquip = 0)
    {
        item = _item;
        amount = _amount;
        itemId = _id;
        isEquiped = false;
        item.itemId = _id;
        indexEquip = _indexEquip;
    }
    public void addAmount(int val)
    {
        amount += val;
    }
}
