﻿
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Circle Profile", menuName = "UI/circleCombo/circle 1")]
public class circleComboObj : ScriptableObject
{
    public masteryManager _mM;
    public int successCount, failCount;
    public Sprite mainSprite;
    public Sprite ringSprite;

    public void increaseSuccess()
    {
        successCount++;
        _mM.masterySessionCheck();
    }
    public void increaseFail()
    {
        failCount++;
        _mM.masterySessionCheck();
    }
    public void newSession()
    {
        successCount = 0;
        failCount = 0;
    }
}
