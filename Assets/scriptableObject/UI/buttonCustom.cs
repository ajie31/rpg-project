﻿
using UnityEngine;
using UnityEngine.UI;


[CreateAssetMenu(fileName = "New Button", menuName = "UI/ButtonSkill/button")]
public class buttonCustom : ScriptableObject
{
    public string buttonName;
    public string idUpgrade;
    public string upgradeRequirement;
    public string idUpgradeReq;
    public int upgradeReqTier;
    public int indexUpgrade;
    public int perkLevelReq;
    public int skillPointCost;
    public Sprite spriteSheet;

    public void checkButtonStats(Button _button)
    {
        if (PlayerPrefs.GetInt(idUpgrade) >= indexUpgrade)
        {
            _button.interactable = false;
        }
    }
}
