﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "new Weapon Manager", menuName = "Player/weapon manager")]
public class playerWeaponManager : ScriptableObject
{
    [Header("General Properties")]
    public playerStatusOBJ _pStatus;
    public DamageIO _pDamageIO;

    [Header("Weapon Selection")]
    public weaponObject[] _weaponChosen;
    public weaponObject _weaponEquiped;

    [SerializeField] private weaponClass[] enumSetWeapon;
    private weaponClass[] enumSetWeapon1 = new weaponClass[2];
    weaponClass[] enumDifference;

    public void weaponSetup()
    {
        _pStatus._weaponClass = _weaponChosen[0]._weaponClass;
        _pStatus.getAttribute[attributes.Attack].basicVal = _weaponChosen[0]._attBonus[0].bonusAtt;
        enumSetWeapon1[0] = _weaponChosen[0]._weaponClass;
        enumSetWeapon1[1] = _weaponChosen[1]._weaponClass;
    }
    #region Weapon change in HUD
    public void changeWeapon(bool _choose)
    {
        _weaponEquiped = _weaponChosen[0];
        _weaponChosen[0] = _weaponChosen[1];
        _weaponChosen[1] = _weaponEquiped;
        _pStatus._weaponClass = _weaponChosen[0]._weaponClass;
        _pStatus.getAttribute[attributes.Attack].basicVal = _weaponChosen[0]._attBonus[0].bonusAtt;
        _pStatus.resetTotalValue(attributes.Attack);
        enumSetWeapon1[0] = _weaponChosen[0]._weaponClass;
        enumSetWeapon1[1] = _weaponChosen[1]._weaponClass;

    }
    #endregion

    #region calculating Attribute by Damage
    public void attributeBasicBalance(float damage)
    {
        increaseBaseAttribute(damage);
        decreaseSecondaryAttribute();
        decreaseUnequipAttribute();
    }

    public void increaseBaseAttribute(float damage)
    {
        switch (_weaponChosen[0]._weaponClass)
        {
            case weaponClass.sword:
                _pStatus.getAttribute[attributes.strength].basicVal += attributeIncrement(attributes.strength, damage);

                if (_pStatus.getAttribute[attributes.strength].basicVal > _pStatus.getAttribute[attributes.strength].maxValue)
                {
                    _pStatus.getAttribute[attributes.strength].basicVal = _pStatus.getAttribute[attributes.strength].maxValue;
                }
                _pStatus.resetTotalValue(attributes.strength);
                _pStatus.bonusStrArmor();
                break;
            case weaponClass.dagger:
                _pStatus.getAttribute[attributes.agility].basicVal += attributeIncrement(attributes.agility, damage);

                if (_pStatus.getAttribute[attributes.agility].basicVal > _pStatus.getAttribute[attributes.agility].maxValue)
                {
                    _pStatus.getAttribute[attributes.agility].basicVal = _pStatus.getAttribute[attributes.agility].maxValue;
                }
                _pStatus.resetTotalValue(attributes.agility);
                break;
            case weaponClass.range:
                _pStatus.getAttribute[attributes.agility].basicVal += attributeIncrement(attributes.agility, damage);

                if (_pStatus.getAttribute[attributes.agility].basicVal > _pStatus.getAttribute[attributes.agility].maxValue)
                {
                    _pStatus.getAttribute[attributes.agility].basicVal = _pStatus.getAttribute[attributes.agility].maxValue;
                }
                _pStatus.resetTotalValue(attributes.agility);
                break;
            case weaponClass.wizard:
                _pStatus.getAttribute[attributes.intelegent].basicVal += attributeIncrement(attributes.intelegent, damage);

                if (_pStatus.getAttribute[attributes.intelegent].basicVal > _pStatus.getAttribute[attributes.intelegent].maxValue)
                {
                    _pStatus.getAttribute[attributes.intelegent].basicVal = _pStatus.getAttribute[attributes.intelegent].maxValue;
                }
                _pStatus.resetTotalValue(attributes.intelegent);
                break;
            default:
                break;
        }

    }

    #endregion

    #region attribute Calculation
    public float attributeIncrement(attributes _attrib, float damage)
    {
        float a = damage * (0.0001f - ((_pStatus._playerPerk[(int)_pStatus._weaponClass]._perkLevel - 1) * 0.000001f));

        return a;
    }

    public void decreaseSecondaryAttribute()
    {
        float a;
        switch (_weaponChosen[1]._weaponClass)
        {

            case weaponClass.sword:
                a = _pStatus.getAttribute[attributes.strength].basicVal;
                _pStatus.getAttribute[attributes.strength].basicVal -= ((a - 10f) * 0.0001f);
                if (_pStatus.getAttribute[attributes.strength].basicVal < 10)
                {
                    _pStatus.getAttribute[attributes.strength].basicVal = 10;
                }
                break;
            case weaponClass.dagger:
                a = _pStatus.getAttribute[attributes.agility].basicVal;
                _pStatus.getAttribute[attributes.agility].basicVal -= ((a - 10f) * 0.0001f);
                if (_pStatus.getAttribute[attributes.agility].basicVal < 10)
                {
                    _pStatus.getAttribute[attributes.agility].basicVal = 10;
                }
                break;
            case weaponClass.range:
                a = _pStatus.getAttribute[attributes.agility].basicVal;
                _pStatus.getAttribute[attributes.agility].basicVal -= ((a - 10f) * 0.0001f);
                if (_pStatus.getAttribute[attributes.agility].basicVal < 10)
                {
                    _pStatus.getAttribute[attributes.agility].basicVal = 10;
                }
                break;
            case weaponClass.wizard:
                a = _pStatus.getAttribute[attributes.intelegent].basicVal;
                _pStatus.getAttribute[attributes.intelegent].basicVal -= ((a - 10f) * 0.0001f);
                if (_pStatus.getAttribute[attributes.intelegent].basicVal < 10)
                {
                    _pStatus.getAttribute[attributes.intelegent].basicVal = 10;
                }
                break;
            default:
                break;
        }
    }

    public void decreaseUnequipAttribute()
    {
        enumDifference = (from num in enumSetWeapon
                          select num)
                  .Except(enumSetWeapon1).ToArray();

        foreach (var item in enumDifference)
        {
            if (item == weaponClass.sword)
            {
                _pStatus.getAttribute[attributes.strength].basicVal -= 0.0001f;
                if (_pStatus.getAttribute[attributes.strength].basicVal < 10)
                {
                    _pStatus.getAttribute[attributes.strength].basicVal = 10;
                }
            }
            if (item == weaponClass.dagger && item == weaponClass.range)
            {

                _pStatus.getAttribute[attributes.agility].basicVal -= 0.0001f;
                if (_pStatus.getAttribute[attributes.agility].basicVal < 10)
                {
                    _pStatus.getAttribute[attributes.agility].basicVal = 10;
                }
            }
            if (item == weaponClass.wizard)
            {

                _pStatus.getAttribute[attributes.intelegent].basicVal -= 0.0001f;
                if (_pStatus.getAttribute[attributes.intelegent].basicVal < 10)
                {
                    _pStatus.getAttribute[attributes.intelegent].basicVal = 10;
                }
            }
        }
    }
    #endregion
}


