﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "new healing Player object", menuName = "Player/status/Healing object")]
public class healingSystem : ScriptableObject
{
    public playerStatusOBJ _pStatus;

    public void heal(float point)
    {       
        if ((_pStatus.getAttribute[attributes.Health].actualValue + point) < _pStatus.getAttribute[attributes.Health].totalValue)
        {
            _pStatus.getAttribute[attributes.Health].actualValue += point;
        }
        else
        {
            _pStatus.getAttribute[attributes.Health].actualValue = _pStatus.getAttribute[attributes.Health].totalValue;
        }       
        playerUIManager._pUIManager.updateHPVal();
    }

    public void manaIncrease(float point)
    {

        if (_pStatus.getAttribute[attributes.ManaPoint].actualValue + point < _pStatus.getAttribute[attributes.ManaPoint].totalValue)
        {
            _pStatus.getAttribute[attributes.ManaPoint].actualValue += point;
           
        }
        else 
        {
            _pStatus.getAttribute[attributes.ManaPoint].actualValue = _pStatus.getAttribute[attributes.ManaPoint].totalValue;
        }
        playerUIManager._pUIManager.updateManaVal();
    }

    public void ManaDecrease(float point)
    {
        if (_pStatus.getAttribute[attributes.ManaPoint].actualValue > 0)
        {
            _pStatus.getAttribute[attributes.ManaPoint].actualValue -= point;
            if (_pStatus.getAttribute[attributes.ManaPoint].actualValue <= 0)
            {
                _pStatus.getAttribute[attributes.ManaPoint].actualValue = 0;
            }
        }

        playerUIManager._pUIManager.updateManaVal();
    }
}
