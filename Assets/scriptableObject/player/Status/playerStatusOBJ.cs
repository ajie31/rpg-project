﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Status Player object", menuName = "Player/status/status object")]
public class playerStatusOBJ : ScriptableObject
{
    [Header("Player Attributes")]
    public int playerBaseAttribMax;
    public playerAttribute[] _attributes;
    public Dictionary<attributes, playerAttribute> getAttribute = new Dictionary<attributes, playerAttribute>();
    public AnimationClip _attackAnimation;

    [Header("Player Focus")]
    public float focusPoint;
    public float totalFocusPoint;

    #region player XP & perk property
    [Header("Player XP")]
    public float playerExp;
    public int maxXp;
    public int baseMaxXp;
    public int playerLvl = 1;
    [Header("Player Perk")]
    public playerPerk[] _playerPerk;
    public int maxPerkInitial;
    public float perkMaxCalculation;
    
    public int playerSkillPt = 0;

    #endregion
    [Header("Player Weapon Class")]
    public weaponClass _weaponClass;

    [Header("Player dead or life")]
    public bool isPlayerDead;

    #region attribute general management
    public void setupDictionary()
    {
        getAttribute = new Dictionary<attributes, playerAttribute>();
        for (int i = 0; i < _attributes.Length; i++)
        {
            getAttribute.Add(_attributes[i]._attr, _attributes[i]);

        }
    }

    public void resetActual(attributes attr)
    {
        getAttribute[attr].actualValue = getAttribute[attr].totalValue;
    }

    public void resetTotalValue(attributes attr)
    {
        getAttribute[attr].totalValue = getAttribute[attr].basicVal + getAttribute[attr].bonusValue + getAttribute[attr].bonusStatus;
    }

    public void decreaseTotalValue(attributes attr, float val)
    {
        getAttribute[attr].totalValue = getAttribute[attr].totalValue - val;
    }

    public float percentageValue(attributes attr)
    {
        return getAttribute[attr].actualValue / getAttribute[attr].totalValue;
    }

    public void resetIsCompared()
    {
        foreach (var attrib in _attributes)
        {
            attrib.isCompare = false;
            attrib.valueComparison = 0;
        }
    }

    public void editComparisonValue(playerAttribute _attrib, float value)
    {
        _attrib.valueComparison += value;

    }

    #endregion


    #region strength bonus
   /* public void bonusStrHP()
    {
        float z = getAttribute[attributes.strength].totalValue / getAttribute[attributes.Health].maxValue;
        getAttribute[attributes.Health].basicVal += (getAttribute[attributes.Health].totalValue * z);
        resetTotalValue(attributes.Health);
        playerUIManager._pUIManager.updateHPVal();
    }*/
    public void bonusStrArmor()
    {
        float z = getAttribute[attributes.strength].totalValue * 0.1f;
        getAttribute[attributes.Armor].bonusStatus = z;
        resetTotalValue(attributes.Armor);
        
    }
    #endregion

    #region focusRelated
    public float getFocusStatus()
    {
        if (focusPoint > 0)
        {
            return focusPoint;
        }
        else
        {
            return 0;
        }
    }

    public void setFocusValue(float value)
    {

        if (focusPoint >= totalFocusPoint)
        {
            focusPoint = totalFocusPoint;
        }
        else
        {
            focusPoint += value;
        }

    }

    public float getFocusRatio()
    {

        if (focusPoint > 0)
        {
            return focusPoint / totalFocusPoint;
        }
        else
        {
            return 0;
        }
    }

    public void useFocusPoint()
    {
        if (getFocusRatio() >= 1)
        {
            focusPoint = 0;
        }
        
    }
    #endregion

    #region XP & Perk related
    public float getExpPercent()
    {
       
        return playerExp / maxXp;
    }
    public float getExp()
    {
        return playerExp;
    }
    public float getPerkPercent()
    {
       
        return _playerPerk[(int)_weaponClass]._PerkProgress / _playerPerk[(int)_weaponClass]._maxPerk;

    }
    public float getPerk()
    {
        return _playerPerk[(int)_weaponClass]._PerkProgress;
    }
    public int getPlayerLevel()
    {
        return playerLvl;
    }

    public void setMaxPerk()
    {
       perkMaxCalculation = maxPerkInitial *
            (Mathf.Pow(_playerPerk[(int)_weaponClass]._perkLevel, 0.4f));

       _playerPerk[(int)_weaponClass]._maxPerk = (int)perkMaxCalculation;
    }


    #endregion

}
[System.Serializable]
public class playerAttribute : ISerializationCallbackReceiver
{    
    public string name;
    public attributes _attr;
    public int maxValue;
    public float actualValue;
    public float bonusStatus;
    public float basicVal;
    public float bonusValue;
    public float totalValue;
    public float valueComparison;
    public bool isCompare;

    public playerAttribute(attributes _attribute, float _actualValue,float _bonusStatus, float _basicValue,float _bonusVal,float _totalVal,float compareVal,bool _iscompare)
    {
        _attr = _attribute;
        actualValue = _actualValue;
        bonusStatus = _bonusStatus;
        basicVal = _basicValue;
        bonusValue =_bonusVal;
        totalValue = _totalVal;
        valueComparison = compareVal;
        isCompare = _iscompare;
        
    }
    public void OnBeforeSerialize()
    {
        switch (_attr)
        {
            case attributes.Health:
                name = "Health";
                break;
            case attributes.Armor:
                name = "Armor";
                break;
            case attributes.Attack:
                name = "Attack";
                break;
            case attributes.MovementSpd:
                name = "Movement.S";
                break;
            case attributes.AttackSpd:
                name = "Attack.S";
                break;
            case attributes.Evasion:
                name = "Evasion";
                break;
            case attributes.ManaPoint:
                name = "ManaPoint";
                break;
            case attributes.Crits:
                name = "Crits%";
                break;
            case attributes.strength:
                name = "Strength";
                break;
            case attributes.agility:
                name = "Agility";
                break;
            case attributes.intelegent:
                name = "Intelegent";
                break;
            default:
                name = " ";
                break;
        }
        if (basicVal != 0)
        {
            totalValue = basicVal + bonusValue + bonusStatus;
        }
    }

    public void OnAfterDeserialize()
    {

    }
}
 
public enum weaponClass
{
    sword,
    dagger,
    range,
    wizard
}

[System.Serializable]
public class playerPerk
{
    public string perkName;
    public float _PerkProgress;
    public int _maxPerk;
    public int _perkLevel;
    public playerPerk(string _perkName, float progress, int max,int level)
    {
        perkName = _perkName;
        _PerkProgress = progress;
        _maxPerk = max;
        _perkLevel = level;
    }
}