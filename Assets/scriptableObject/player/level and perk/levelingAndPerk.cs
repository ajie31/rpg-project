﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new level and perk object", menuName = "Player/status/level and perk object")]
public class levelingAndPerk : ScriptableObject
{
   public playerStatusOBJ _pStatus;
    public int perkGain; 
    public float multiplierKS;
    public int killStk;

    private float perkCalculation, perkMaxExponent;
    private float levelXpExponent;

   public void increaseXp()
   {
        _pStatus.playerExp++; ;
        checkPlayerExp();
   }

    private void checkPlayerExp()
    {
        if (_pStatus.getExpPercent() >= 1)
        {
            _pStatus.playerSkillPt++;
            _pStatus.playerLvl++;
            _pStatus.playerExp = 0;
            levelXpExponent = _pStatus.baseMaxXp * (Mathf.Pow(_pStatus.playerLvl, 0.5f));
            _pStatus.maxXp += (int)levelXpExponent;
            playerUIManager._pUIManager.updatePlayerLevel();
        }
        playerUIManager._pUIManager.updateXpMeter();
    }

    public void increasePerk()
    {
        //_pStatus.PerkProgress++;
        perkCalculation = perkGain * (1 + multiplierKS * killStk);
        _pStatus._playerPerk[(int)_pStatus._weaponClass]._PerkProgress += perkCalculation;
        checkPlayerPerk();
    }

    public void checkPlayerPerk()
    {
        if (_pStatus.getPerkPercent() >= 1)
        {
            _pStatus._playerPerk[(int)_pStatus._weaponClass]._perkLevel++;
            _pStatus._playerPerk[(int)_pStatus._weaponClass]._PerkProgress = 0;

            _pStatus.setMaxPerk();
            playerUIManager._pUIManager.updatePerkLvl(_pStatus._playerPerk[(int)_pStatus._weaponClass]._perkLevel);
        }
       
        playerUIManager._pUIManager.updatePerkMeter();
    }

    public void decreaseSkillPoint(int val)
    {
        _pStatus.playerSkillPt -= val;
    }
}
