﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new DamageI/O Player object", menuName = "Player/status/DamageIO object")]
public class DamageIO : ScriptableObject
{
    public playerStatusOBJ _pStatus;   
    public float damageTaken;
    public float masteryBonusDamage_Mul;
    public playerWeaponManager _pWeaponM;

    #region damage Input
    public void damageInput(float attack)
    {
        
        if (_pStatus.getAttribute[attributes.Health].actualValue > 0 )//&& !_pStatus.isImunePhys)
        {
            damageTaken = attack * (100 / (100 + _pStatus.getAttribute[attributes.Armor].totalValue));
            _pStatus.getAttribute[attributes.Health].actualValue = _pStatus.getAttribute[attributes.Health].actualValue - damageTaken;
            if (_pStatus.getAttribute[attributes.Health].actualValue <= 0)
            {
                _pStatus.getAttribute[attributes.Health].actualValue = 0;
            }
        }
        playerUIManager._pUIManager.updateHPVal();
        if (_pStatus.focusPoint < 1)
        {
            _pStatus.setFocusValue(5);
            playerUIManager._pUIManager.updateFocusVal();
        }
    }

    public void getDamageInputPure(float damage)
    {
        _pStatus.getAttribute[attributes.Health].actualValue -= damage;
        playerUIManager._pUIManager.updateHPVal();
        if (_pStatus.focusPoint < 1)
        {
            _pStatus.setFocusValue(5);
            playerUIManager._pUIManager.updateFocusVal();
        }
    }
    #endregion
    #region damage output
    public float getBasicDamage()
    {

        return _pStatus.getAttribute[attributes.Attack].totalValue;

    }

    public float damageModOutput(float modifier = 1,float instant = 0)
    {
        if (instant <= 0)
        {
            return (_pStatus.getAttribute[attributes.Attack].totalValue * modifier) + masteryBonusDamageFormula();
        }
        else
        {
            return instant;
        }
    }
    #endregion

    private float masteryBonusDamageFormula()
    {
        return masteryBonusDamage_Mul * _pStatus.getAttribute[attributes.Attack].totalValue;
    }

    public void damageRecieveFromEnemy(float damage)
    {
        _pWeaponM.attributeBasicBalance(damage);
    }
    
}
