﻿
using UnityEngine;

[CreateAssetMenu(fileName ="New Skill",menuName = "Skills/ability") ]
public class skill : ScriptableObject
{
    public string skillName;

    [TextArea]
    public string description;

    public Sprite artwork;

    public Sprite treeSprite;
    public int[] upgradeCost;
    public float[] coolDown;

    public Vector2[] patternCombo;

    public float[] damageMult;
    public float[] damageInstant;

    public float[] movementSpeed;
    public float[] attackSpeed;

    public float[] debuffMovementSpeed;
    public float[] debuffAttackSpeed;

    public float[] manaCost;

    public float[] duration;
    public float[] delayDuration;
    public float[] stunDuration;
    public float[] durationDebuff;

    public float[] heal;
    public float[] increaseHealth;

    public bool needTarget;
    public bool isArea;
    public float[] areaRange;
    public float[] areaDuration;

    public bool isMastery;
    public bool masteryRequirement;

    public delegate void skillDelegate();
    public skillDelegate m_skillAction;

    public void registerSkillAction(skillDelegate method)
    {
        m_skillAction = method;
    }

    public void skillInAction()
    {
        m_skillAction();

    }



}
