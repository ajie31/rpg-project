﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "New Skill Group", menuName = "Player/Player Register")]
public class playerReg : ScriptableObject
{
    public playerStatusOBJ _pStatus;
    public healingSystem _heal;
    public statusEffect _statusEffect;
    public DamageIO _damageIO;

}
