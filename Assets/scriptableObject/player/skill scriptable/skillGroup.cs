﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Skill Group", menuName = "Skills/skill group")]
public class skillGroup : ScriptableObject
{
    public skillObj[] _skill;
}
