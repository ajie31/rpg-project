﻿
using UnityEngine;
[CreateAssetMenu(fileName = "New Skill ", menuName = "Skills/Sword/Hell Wrath")]
public class hellWrath : skillObj
{
    public playerReg _pReg;
    public string upgradeMessage;
    public float[] manaCostHell;
    public float[] damageInstant;
    public float areaRange;
    public bool isHellWrath;
    [SerializeField]private float hellWrathDuration = 3.5f;
    public float hellwrathDurationCount;
    public int hellwrathPerSec;
    public int damageInstantUp, manacostUp;

    public void castPower(playerCollider _pCollider)
    {
        if ((_pReg._pStatus.getAttribute[attributes.ManaPoint].actualValue >= manaCostHell[manacostUp] || isMastery))
        {
            hellwrathDurationCount = hellWrathDuration;
            skillMan._skillsManager.setCooldown(coolDown);
            masteryRequirement = true;
            //_pCollider.prepareToAtt = false;
            _pCollider.setIsSkill(true);

            if (!isMastery)
            {
                _pReg._heal.ManaDecrease(manaCostHell[manacostUp]);
            }
           
            isHellWrath = true;
            _pReg._statusEffect.isNoInterupt = true;
            

        }
        else
        {
            skillMan._skillsManager.setCooldown(0);
            masteryRequirement = false;
            skillMan._skillsManager._castStatus = castStatus.noMana;
        }
    }

    public void damageOut(combatScript _combat)
    {
        _combat.skillArea(true, areaRange, damageInstant[damageInstantUp]);
    }

    public void skillOver(playerCollider _pCollider)
    {

            isHellWrath = false;
            hellwrathDurationCount = 0;
            _pReg._statusEffect.isNoInterupt = false;
            _pCollider.setIsSkill(false);

    }

    public void upgradeSkill(int _skillBranch, int upgrade, bool confirmation)
    {
        if (_skillBranch == 1)
        {
            if (!confirmation)
            {
                upgradeMessage = "Damage increase" + damageInstant[upgrade].ToString();
            }
            else
            {
                damageInstantUp = upgrade;
                PlayerPrefs.SetInt("damageInstantUp5", upgrade);
            }

        }
        else if (_skillBranch == 2)
        {
            if (!confirmation)
            {
                upgradeMessage = "Mana Cost" + manaCostHell[upgrade].ToString();
            }
            else
            {
                manacostUp = upgrade;
                PlayerPrefs.SetInt("manacostUp5", upgrade);
            }

        }

    }
    public string displayDescription()
    {
        return "Damage increase " + damageInstant[damageInstantUp].ToString() +
            ", Mana Cost " + manaCostHell[manacostUp].ToString();
    }

}
