﻿
using UnityEngine;
[CreateAssetMenu(fileName = "New Skill ", menuName = "Skills/Sword/Soul Render")]
public class soulRender : skillObj
{
    public playerReg _pReg;
    public string upgradeMessage;

    public float[] healing;
    public float[] healthCost;
    public float[] soulRenderDuration;
    public bool isSoulRender;
   // public float _soulRenderDuration;
    public int healthCostUp, lifestealUp, durationUp;

    public void castPower(playerCollider _pCollider, combatScript _combat)
    {
        skillMan._skillsManager.setCooldown(coolDown);
        masteryRequirement = true;
        //soulRenderDuration = _skill[3].duration[durationUp4];
        if (!isMastery)
        {
            _pReg. _damageIO.getDamageInputPure(_pReg._pStatus.getAttribute[attributes.Health].actualValue * healthCost[healthCostUp]);
        }
        isSoulRender = true;
        _combat.setLifeSteal(true, healing[lifestealUp]);
        _pCollider.setIsSkill(true);

    }

    public float getDuration()
    {
        return soulRenderDuration[durationUp];
    }

    public void skillOver(combatScript _combat)
    {

        _combat.setLifeSteal(false, 0);
        isSoulRender = false;
        
    }

    public void upgradeSkill(int _skillBranch, int upgrade, bool confirmation)
    {
        if (_skillBranch == 1)
        {
            if (!confirmation)
            {
                upgradeMessage = "Decrease health Sacrafice multiplier " + healthCost[upgrade].ToString();
            }
            else
            {
                healthCostUp = upgrade;
                PlayerPrefs.SetInt("healthCostUp4", upgrade);
            }

        }
        else if (_skillBranch == 2)
        {
            if (!confirmation)
            {
                upgradeMessage = "Life Steal Heal " + healing[upgrade].ToString();
            }
            else
            {
                lifestealUp = upgrade;
                PlayerPrefs.SetInt("lifestealUp4", upgrade);
            }

        }
        else if (_skillBranch == 3)
        {
            if (!confirmation)
            {
                upgradeMessage = "Effect Duration " + soulRenderDuration[upgrade].ToString();
            }
            else
            {
                durationUp = upgrade;
                PlayerPrefs.SetInt("durationUp4", upgrade);
            }

        }

    }

    public string displayDescription()
    {
        return "Decrease health Sacrafice multiplier " + healthCost[healthCostUp].ToString() +
            ", Life Steal Heal " + healing[lifestealUp].ToString() +
          ", Effect Duration " + soulRenderDuration[durationUp].ToString();
    }

}
