﻿
using UnityEngine;
[CreateAssetMenu(fileName = "New Skill ", menuName = "Skills/Sword/True Strike")]
public class trueStrike : skillObj
{
    public playerReg _pReg;
    public string upgradeMessage;

    public float[] durationBuffAsp;
    public float[] damageMult;
    public float[] durationDebuff;

    public bool isAspBuff;

    public float attackSpeed;

    public int damageMulUp, attSpeedDurUp, silentDurUp;

    public void castPower(playerCollider _pCollider, combatScript _combat)
    {

        if ((_pReg._pStatus.getAttribute[attributes.ManaPoint].actualValue >= manaCost || isMastery))
        {
            _pCollider.setIsSkill(true);
           
            skillMan._skillsManager.setCooldown(coolDown);
            masteryRequirement = true;
            
            _combat.setMultiplyDamage(damageMult[damageMulUp]);
            _combat.setStunDur(1f);

            _pReg._statusEffect.setAttackSpeed(attackSpeed,durationBuffAsp[attSpeedDurUp]);

            isAspBuff = true;
            if (!isMastery)
            {
                _pReg._heal.ManaDecrease(manaCost);
            }
            _combat.setSilentweapon(true, durationDebuff[silentDurUp]);

            _pCollider.attack(true, true, true, false, true);
        }
        else
        {
            skillMan._skillsManager.setCooldown(0);
            masteryRequirement = true;
            skillMan._skillsManager._castStatus = castStatus.noMana;
           
        }
    }

    public float getDuration()
    {
        return durationBuffAsp[attSpeedDurUp];
    }

    public void upgradeSkill(int _skillBranch, int upgrade, bool confirmation)
    {

        if (_skillBranch == 1)
        {
            if (!confirmation)
            {
                upgradeMessage = "Increase Damage Multiplier " + damageMult[upgrade].ToString();
            }
            else
            {
                damageMulUp = upgrade;
                PlayerPrefs.SetInt("damageMulUp2", upgrade);
            }

        }
        else if (_skillBranch == 2)
        {
            if (!confirmation)
            {
                upgradeMessage = "Attack Speed Duration " + durationBuffAsp[upgrade].ToString();
            }
            else
            {
                attSpeedDurUp = upgrade;
                PlayerPrefs.SetInt("attSpeedDurUp2", upgrade);
            }

        }
        else if (_skillBranch == 3)
        {
            if (!confirmation)
            {
                upgradeMessage = "Silent Duration " + durationDebuff[upgrade].ToString();
            }
            else
            {
                silentDurUp = upgrade;
                PlayerPrefs.SetInt("silentDurUp2", upgrade);
            }

        }

    }

    public string displayDescription()
    {
        return ("Increase Damage Multiplier " + damageMult[damageMulUp].ToString() +
            ", Attack Speed Duration " + durationBuffAsp[attSpeedDurUp].ToString() +
            ", Silent Duration " + durationDebuff[silentDurUp].ToString());
    }

}
