﻿using UnityEngine;
[CreateAssetMenu(fileName = "New Skill ", menuName = "Skills/Sword/Demon Blood")]
public class demonBlood : skillObj
{
    public playerReg _pReg;
    public string upgradeMessage;

    public float[] increaseHealth;
    public float[] delayDuration;
    public float[] rangeExplotion;
    public float damageMult;
    public int hpIncreaseUp, rangeExplotionUp, delayExplotionUp;
    public float stunDuration;
    //public float[] coolDown;

    public bool isExplode;
    

    public void castPower(playerCollider _pCollider, combatScript _combat)
    {
        if ((_pReg._pStatus.getAttribute[attributes.ManaPoint].actualValue >= manaCost || isMastery))
        {
            skillMan._skillsManager.setCooldown(coolDown);
            masteryRequirement = true;
            _pCollider.setIsSkill(true);
            _combat.setMultiplyDamage(0);
            if (!isMastery)
            {
                _pReg._heal.ManaDecrease(manaCost);
            }

            _combat.setStunDur(stunDuration);
           
            _pReg._statusEffect.totalHealthIncrease(increaseHealth[hpIncreaseUp]);
            isExplode = true;
            playerUIManager._pUIManager.updateHPVal();
        }
        else
        {
            skillMan._skillsManager.setCooldown(0);
            masteryRequirement = false;
            skillMan._skillsManager._castStatus = castStatus.noMana;
            //  StartCoroutine(skillMan._skillsManager.skillStatusTimer());
        }

    }

    public float getDelay()
    {
        return delayDuration[delayExplotionUp];
    }

    public void skillOver(combatScript _combat)
    {
        _combat.skillArea(true, rangeExplotion[rangeExplotionUp], _pReg._statusEffect.getDeltaHealth() *damageMult * _pReg._pStatus.getAttribute[attributes.Attack].totalValue);
            //explode here
        _pReg._statusEffect.resettotalHealthPoint();

        isExplode = false;
        playerUIManager._pUIManager.updateHPVal();

    }

    public void upgradeSkill(int _skillBranch, int upgrade, bool confirmation)
    {

        if (_skillBranch == 1)
        {
            if (!confirmation)
            {
                upgradeMessage = "Increase Health " + increaseHealth[upgrade].ToString();
            }
            else
            {
                hpIncreaseUp = upgrade;
               // PlayerPrefs.SetInt("hpIncreaseUp1", upgrade);
            }

        }
        else if (_skillBranch == 2)
        {
            if (!confirmation)
            {
                upgradeMessage  = "increase range Explotion " + rangeExplotion[upgrade].ToString();
            }
            else
            {
                rangeExplotionUp = upgrade;
                //PlayerPrefs.SetInt("rangeExplotionUp1", upgrade);
            }

        }
        else if (_skillBranch == 3)
        {
            if (!confirmation)
            {
                upgradeMessage = "reduce delay explotion duration " + delayDuration[upgrade].ToString();
            }
            else
            {
                delayExplotionUp = upgrade;
                PlayerPrefs.SetInt("delayExplotion1", upgrade);
            }

        }

    }

    public string displayDescription()
    {

        return ("Increase Health " + increaseHealth[hpIncreaseUp].ToString() +
            ", increase range Explotion " + rangeExplotion[rangeExplotionUp].ToString() +
             ", reduce delay explotion duration " + delayDuration[delayExplotionUp].ToString());
    }

}
