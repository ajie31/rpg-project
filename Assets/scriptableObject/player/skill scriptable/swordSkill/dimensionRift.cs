﻿
using UnityEngine;
[CreateAssetMenu(fileName = "New Skill ", menuName = "Skills/Sword/Dimension Rift")]
public class dimensionRift : skillObj
{
    public playerReg _pReg;
    public string upgradeMessage;

    public float[] delayRiftDuration;
    public float[] durationBuffMsp;
    public float[] movementSpeed;
    public bool isDimensionRift;
    public float durationDebuff;
    public float debuffMovementSpeed;

    public int mspdUp, delayUp, buffDurationUp;

    public void castPower(playerCollider _pCollider, combatScript _combat, playerFollow _pfollow)
    {

        if (_pfollow.selectedObj != null &&
            (_pReg._pStatus.getAttribute[attributes.ManaPoint].actualValue >= manaCost ||
            isMastery))
        {
            skillMan._skillsManager.setCooldown(coolDown);
            _pCollider.setIsSkill(true);
            masteryRequirement = true;

            _pReg._statusEffect.isNoInterupt = true;

            //_pfollow.setSpeed(0);

            isDimensionRift = true;
            if (!isMastery)
            {
                _pReg._heal.ManaDecrease(manaCost);
            }

        }
        else
        {
            skillMan._skillsManager.setCooldown(0);
            masteryRequirement = false;

            if (_pfollow.selectedObj == null)
            {
                skillMan._skillsManager._castStatus = castStatus.noTarget;
            }
            else
            {
                skillMan._skillsManager._castStatus = castStatus.noMana;
            }
        }
    }
    public float getDelayDuration()
    {
        return delayRiftDuration[delayUp];
    }

    public float getBuffDuration()
    {
        return durationBuffMsp[buffDurationUp];
    }

    public void afterBlink(playerCollider _pCollider, combatScript _combat, playerFollow _pfollow)
    {
        isDimensionRift = false;
        _pReg._statusEffect.isNoInterupt = false;

        _pReg._statusEffect.setMovementSpeed(movementSpeed[mspdUp], durationBuffMsp[buffDurationUp]);
        _pReg._statusEffect.setReflectDamage(false, durationBuffMsp[buffDurationUp]);
        
        _pCollider.setIsSkill(false);
        _combat.setInstantDamage(0);

        _combat.setEnemyDebuffSlowMovement(durationDebuff, debuffMovementSpeed);

        _pCollider.transform.position = (_pfollow.selectedObj.GetComponent<enemyAtt>().getBackPost().position);
        _pfollow.setDestination(_pfollow.selectedObj.transform.position);
    }

    public void upgradeSkill(int _skillBranch, int upgrade, bool confirmation)
    {
        if (_skillBranch == 1)
        {
            if (!confirmation)
            {
                upgradeMessage = "Movement Speed Duration " + durationBuffMsp[upgrade].ToString();
            }
            else
            {
                buffDurationUp = upgrade;
                PlayerPrefs.SetInt("buffDurationUp3", upgrade);
            }

        }
        else if (_skillBranch == 2)
        {
            if (!confirmation)
            {
                upgradeMessage = "Movement Speed " + movementSpeed[upgrade].ToString();
            }
            else
            {
                mspdUp = upgrade;
                PlayerPrefs.SetInt("mspdUp3", upgrade);
            }

        }
        else if (_skillBranch == 3)
        {
            if (!confirmation)
            {
                upgradeMessage = "Delay Duration " + delayRiftDuration[upgrade].ToString();
            }
            else
            {
                delayUp = upgrade;
                PlayerPrefs.SetInt("delayUp3", upgrade);
            }

        }

    }


    public string displayDescription()
    {
        return "Movement Speed Duration " + durationBuffMsp[buffDurationUp].ToString() +
            ", Movement Speed " + movementSpeed[mspdUp].ToString() +
          ", Delay Duration " + delayRiftDuration[delayUp].ToString();
    }

}
