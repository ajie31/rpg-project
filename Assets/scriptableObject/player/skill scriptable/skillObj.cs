﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class skillObj : ScriptableObject
{
    #region skill base property
    public string skillName;
    public weaponClass _weaponClass;
    public Sprite artwork;

    [TextArea]
    public string description;

    public int manaCost;

    public Sprite treeSprite;

    public float coolDown;

    public Vector2[] patternCombo;

    public bool isMastery;
    public bool masteryRequirement;

    #endregion

    public delegate void skillDelegate();
    public skillDelegate m_skillAction;

    public void registerSkillAction(skillDelegate method)
    {
        m_skillAction = method;
    }

    public void skillInAction()
    {
        m_skillAction();

    }

}
