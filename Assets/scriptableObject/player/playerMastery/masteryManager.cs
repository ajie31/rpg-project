﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "new Mastery manager", menuName = "Player/Manager/Mastery")]
public class masteryManager : ScriptableObject
{
    [Header("General Property")]
    #region general Property
    public playerStatusOBJ _pStatus;
    public circleComboObj _cObj;
    public statusEffect _statusEffect;
    public DamageIO _damageIO;
    #endregion

    [Header("Masetery Status")]
    #region mastery Status property
    public bool isMasteryActive;
    public bool isMasteryReady;
    #endregion

    [Header("Masetery Skill Array")]
    public skillObj[] chosenMasterySkill;

    [Header("Masetery Session property")]
    #region mastery Session property
    [SerializeField]private int sessionCount = 1;
    private int circleCount;
    private float comboSuccessRate;
    #endregion

    private void OnEnable()
    {
        circleCount = 0;
        sessionCount = 1;
        comboSuccessRate = 0;
    }
    #region mastery trigger method for combo Button
    public void interuptMastery()
    {
        if (isMasteryActive)
        {
            isMasteryActive = false;
            isMasteryReady = false;
            circleCount = 0;
            sessionCount = 1;
            circlePool._circlePool.disabledCombo();
           
        }
    }

    public void activateMasery()
    {

        if (_pStatus.getFocusRatio() >= 1f && getFirstMastery())
        {
            
            isMasteryActive = true;
            _pStatus.useFocusPoint();
            playerUIManager._pUIManager.updateFocusVal();
            circlePool._circlePool.startDarwCircle(chosenMasterySkill[sessionCount].patternCombo);

        }
    }
    #endregion

    /** if a single combo circle is connect or missing, system is checking wheter combo session is end or still-
     * -play on, by counting the max length of combo circle array in combo position inside skill object 
     */

    #region mastery module check
    public void masterySessionCheck()
    {
        //playerAttr._pAttribute.calculateComboRate(_cObj.successCount, chosenMasterySkill[sessionCount].mas);
        if (circleCount == (chosenMasterySkill[sessionCount].patternCombo.Length - 1))
        {
            calculateComboRate();
            _cObj.newSession();
            masterySkillAction();
            if (sessionCount == chosenMasterySkill.Length-1)
            {
                masteryHasComplete();
            }
            else
            {
                nextSesionSkill();
            }
        }
        else
        {
            circleCount++;
        }
    }

    private void nextSesionSkill()
    {
        sessionCount++;
        circleCount = 0;
        circlePool._circlePool.resetActiveCircleList();
        circlePool._circlePool.startDarwCircle(chosenMasterySkill[sessionCount].patternCombo);
    }

    private void masteryHasComplete()
    {
        circleCount = 0;
        sessionCount = 1;
        interuptMastery();
    }

    #endregion

    /** give boolean  at the first chosen skills run to check if skill is fullfilled otherwise mastery won't run
     * Run the skill as mastery from the next chossen skill and pass combo rate to add damage bonus in damage IO
     */
    #region from skill to mastery
    public bool getFirstMastery()
    {
        if (!_statusEffect.isSilent && !_statusEffect.isNoInterupt
            && !_statusEffect.isGotStun)
        {
            chosenMasterySkill[0].isMastery = true;
            chosenMasterySkill[0].skillInAction();

            return chosenMasterySkill[0].masteryRequirement;
        }
        else
        {

            if (_statusEffect.isSilent)
            {
                skillMan._skillsManager._castStatus = castStatus.noMana;
            }
            else
            {
                skillMan._skillsManager._castStatus = castStatus.isNotInterupt;
            }


            return false;
        }

    }

    private void masterySkillAction()
    {
        if (!_statusEffect.isSilent && !_statusEffect.isNoInterupt
             && !_statusEffect.isGotStun)
        {
            _damageIO.masteryBonusDamage_Mul = comboSuccessRate;
            chosenMasterySkill[sessionCount].isMastery = true;
            chosenMasterySkill[sessionCount].skillInAction();
        }
        else
        {
            if (_statusEffect.isSilent)
            {
                skillMan._skillsManager._castStatus = castStatus.noMana;
            }
            else
            {
                skillMan._skillsManager._castStatus = castStatus.isNotInterupt;

            }

        }

    }

    public void calculateComboRate()
    {
        comboSuccessRate = _cObj.successCount / chosenMasterySkill[sessionCount].patternCombo.Length;
    }
}
#endregion

