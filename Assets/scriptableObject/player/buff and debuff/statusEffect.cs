﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SAP2D;
[CreateAssetMenu(fileName = "new Status Effect object", menuName = "Player/status/Status Effect Object")]
public class statusEffect : ScriptableObject
{
    [Header("Player Status")]
    public playerStatusOBJ _pStatus;

    [Header("player can multi hit")]
    public bool multipleTarget;

    [Header("Player Damage reflection")]
    public float reflectDamageDuration;
    public bool isReflectDamage;

    [Header("perform silence")]
    public bool isSilent;

    [Header("Player is Busy")]
    public bool isNoInterupt;

    [Header("Player ASP Buff")]
    public float ASPEffectDuration;
    public bool isASPEffect;

    [Header("Player MSP Buff")]
    public float MSPEffectDuration;
    public bool isMSPEffect;

    [Header("Player Imune to Physical")]
    public bool isImunePhys;
    public float DurationImunePhys;

    [Header("Player got Stun")]
    public bool isGotStun;
    public float gotStunDuration;
    
    [Header("Player is LifeStealing")]
    public bool isHealthSteal;
    public float healthAmount;
    public float initialHP;

    public void setReflectDamage(bool status,float duration)
    {
        isReflectDamage = status;
        reflectDamageDuration = duration;
    }
    public bool getReflectDamage()
    {
        return isReflectDamage;
    }

    #region isHealth
    public void totalHealthIncrease( float health)
    {
        _pStatus.getAttribute[attributes.Health].totalValue += health;

        _pStatus.getAttribute[attributes.Health].actualValue += health;

    }

    public float getDeltaHealth()
    {
        if (_pStatus.getAttribute[attributes.Health].actualValue - (_pStatus.getAttribute[attributes.Health].basicVal + 
            _pStatus.getAttribute[attributes.Health].bonusValue) >= 0)
        {
            return _pStatus.getAttribute[attributes.Health].actualValue - (_pStatus.getAttribute[attributes.Health].basicVal + _pStatus.getAttribute[attributes.Health].bonusValue);
        }
        else
        {
            return 0;
        }
        
    }

    public void resettotalHealthPoint()
    {
        if (_pStatus.getAttribute[attributes.Health].actualValue >= (_pStatus.getAttribute[attributes.Health].basicVal + _pStatus.getAttribute[attributes.Health].bonusValue))
        {
            _pStatus.resetTotalValue(attributes.Health);
            _pStatus.getAttribute[attributes.Health].actualValue = _pStatus.getAttribute[attributes.Health].totalValue;


        }
        else
        {
            _pStatus.resetTotalValue(attributes.Health);
        }
    }

    #endregion

    #region attack speed
    public void setAttackSpeed(float setNum, float duration)
    {
        _pStatus.getAttribute[attributes.AttackSpd].totalValue = setNum;
        ASPEffectDuration = duration;
        isASPEffect = true;
    }


    public void resetAttackSpeed()
    {
        _pStatus.resetTotalValue(attributes.AttackSpd);
    }
    #endregion

    #region movement Speed
    public void setMovementSpeed(float val,float duration, float mult = 1f)
    {
       
        if (mult != 1)
        {
            _pStatus.getAttribute[attributes.MovementSpd].actualValue += (_pStatus.getAttribute[attributes.MovementSpd].totalValue * mult);
        }
        else
        {
            _pStatus.getAttribute[attributes.MovementSpd].actualValue = val;
        }
        MSPEffectDuration = duration;
        if (duration>0)
        {
            isMSPEffect = true;
        }      
    }

    public void setMoveStop(SAP2DAgent agent)
    {
        agent.MovementSpeed = 0;
    }
    public void resetMovementSpeed()
    {
        _pStatus.resetActual(attributes.MovementSpd);
      
    }
    #endregion

    #region imunity
    public void setIsImunePhysics(float duration)
    {
        isImunePhys = true;
        DurationImunePhys = duration;
    }
    public bool getIsImunePhysics()
    {
        return isImunePhys;
    }
    #endregion


    public void setPlayerGotStun(bool status, float duration,playerCollider _pCollider)
    {
        isGotStun = status;
        gotStunDuration = duration;
        _pCollider.disableAttacking();
        setMovementSpeed(0,0);
    }

}
