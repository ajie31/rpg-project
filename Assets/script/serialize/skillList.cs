﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class skillList 
{
    
    public string name;
    public Vector2[] circlePattern;
    public float Damage;
    public float manaCost;
    public UnityEvent skillEvent;
    public bool isMasstery;
}
