﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class combatScript : MonoBehaviour
{
    public healingSystem _heal;
    public levelingAndPerk _levelPerkObject;
    public DamageIO _damageIO;
    public Transform attackPoint;
    public Animator _attAnimation;
    public float attackRange = 0.5f;
    public float areaRange = 0.5f;
    public LayerMask enemyLayer;
    public playerCollider _pcollider;
    public playerFollow _pFollow;
    private enemyAtt _eAttr;

    private bool isLifeSteal;
    private float amountLifeSteal;

    private float multiplier;
    private float damageInstant;

    private float stunDuration;
   // private float getDamage;

    private float timeDebuffSlow;
    private float slowAmount;

    private float silenceDuration;

    private float perkComboTime;
    private bool isComboPerk;

    public void setLifeSteal(bool status,float amount)
    {
        isLifeSteal = status;
        amountLifeSteal = amount;
    }

    public void setEnemyAttribute(enemyAtt _attr)
    {
        _eAttr = _attr;
    }

    public void setEnemyDebuffSlowMovement(float duration, float amount)
    {
        timeDebuffSlow = duration;
        slowAmount = amount;
    }

    public void setInstantDamage(float damage)
    {

        damageInstant = damage;
    }
    
    public void setMultiplyDamage(float damage)
    {

        multiplier = damage;
    }

    public void setSilentweapon(bool status, float duration)
    {

        silenceDuration = duration;
    }

    public void setStunDur(float duration)
    {
        stunDuration = duration;
    }

    private void Update()
    {
        if (perkComboTime > 0)
        {
            perkComboTime -= Time.deltaTime;
        }
        else if (isComboPerk && perkComboTime <= 0)
        {
            perkComboTime = 0;
            _levelPerkObject.multiplierKS = 0;
            isComboPerk = false;
        }
       
    }

    #region melee attack
    public void meleeAtt()
    {

        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayer);

        foreach (var enemy in hitEnemies)
        {
            //  _pWeaponM.increaseBaseAttribute();
            if (isComboPerk)
            {
                perkComboTime = 3f;
            }
            _levelPerkObject.increasePerk();
           _eAttr.damageInput(_damageIO.getBasicDamage());
            if (_eAttr.ratioHealth() <= 0)
            {
                _eAttr.setLifeStats(true);
                _pcollider.disableAttacking();
               
                enemySpawn._enemySpawn.addToDeathList(_eAttr.gameObject);
                enemySpawn._enemySpawn.setRespawnCount();
                _levelPerkObject.increaseXp();

                perkComboTime = 3f;
                _levelPerkObject.multiplierKS++;
                isComboPerk = true;

                if (isLifeSteal)
                {
                    _heal.heal(amountLifeSteal);
                }
                _pFollow.selectedObj = null;
                _pFollow._target = target.none;
            }
        }
    }
    public void skilledMelee(bool isMultiple = false, bool isKnocking = false, bool isStunning = false,
        bool isSlowDebuff = false, bool isSilence = false, bool isInstant = false)
    {

        Collider2D[] skillHitEnemies = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayer);
        foreach (var enemy in skillHitEnemies)
        {
            _levelPerkObject.increasePerk();
            if (isMultiple)
            {

                if (isKnocking)
                {
                    // Debug.Log("knocking");
                    enemy.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;

                    enemy.GetComponent<Rigidbody2D>().AddForce((enemy.transform.position - transform.position) * 200, ForceMode2D.Force);
                    enemy.GetComponent<enemyAtt>().setGotKnocked(true);
                    //knockTime = .5f;
                }
                if (isStunning)
                {
                    enemy.GetComponent<enemyAtt>().setStunEnemy(stunDuration);
                }

                if (isSlowDebuff)
                {
                    enemy.GetComponent<enemyAtt>().activateSlowDebuff(timeDebuffSlow, slowAmount);

                }


                if (isSilence)
                {
                    enemy.GetComponent<enemyAtt>().setSilent(true, silenceDuration);
                }


                if (isInstant)
                {
                    enemy.GetComponent<enemyAtt>().damageInput(_damageIO.damageModOutput(1, damageInstant));
                }
                else
                {
                    enemy.GetComponent<enemyAtt>().damageInput(_damageIO.damageModOutput(multiplier));
                }

                if (enemy.GetComponent<enemyAtt>().ratioHealth() <= 0)
                {
                    enemy.GetComponent<enemyAtt>().setLifeStats(true);
                    _pcollider.disableAttacking();
                   
                    enemySpawn._enemySpawn.addToDeathList(enemy.gameObject);
                    enemySpawn._enemySpawn.setRespawnCount();

                    _levelPerkObject.increaseXp();

                    perkComboTime = 3f;
                    _levelPerkObject.multiplierKS++;
                    isComboPerk = true;
                    if (isLifeSteal)
                    {
                        _heal.heal(amountLifeSteal);
                    }
                }
            }
            else
            {

                if (isKnocking)
                {

                    _eAttr.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
                    _eAttr.GetComponent<Rigidbody2D>().AddForce((enemy.transform.position - transform.position) * 200, ForceMode2D.Force);
                    _eAttr.GetComponent<enemyAtt>().setGotKnocked(true);
                }
                if (isStunning)
                {
                    _eAttr.setStunEnemy(stunDuration);
                }

                if (isSlowDebuff)
                {

                    _eAttr.activateSlowDebuff(timeDebuffSlow, slowAmount);

                }
                if (isSilence)
                {
                    enemy.GetComponent<enemyAtt>().setSilent(true, silenceDuration);
                }
                if (isInstant)
                {
                   _eAttr.damageInput(_damageIO.damageModOutput(multiplier));
                }
                else
                {
                   _eAttr.damageInput(_damageIO.damageModOutput(multiplier));
                }

                if (_eAttr.ratioHealth() <= 0)
                {
                    _eAttr.setLifeStats(true);
                    _pcollider.disableAttacking();
                    
                    enemySpawn._enemySpawn.addToDeathList(_eAttr.gameObject);
                    enemySpawn._enemySpawn.setRespawnCount();

                    perkComboTime = 3f;
                    _levelPerkObject.multiplierKS++;
                    isComboPerk = true;
                    _levelPerkObject.increaseXp();
                    if (isLifeSteal)
                    {
                        _heal.heal(amountLifeSteal);
                    }

                }

            }
        }
    }

    #endregion

    public void skillArea(bool isStunning = false,float area = 0.5f ,float damage = 0)
    {
        Collider2D[] areaHitEnemies = Physics2D.OverlapCircleAll(transform.position, area, enemyLayer);

        foreach (var enemy in areaHitEnemies)
        {
            _levelPerkObject.increasePerk();
            enemy.GetComponent<enemyAtt>().damageInput(damage);
            
            if (isStunning)
            {
                enemy.GetComponent<enemyAtt>().setStunEnemy(stunDuration);
               // playerAttr._pAttribute.setStun(false, 0);
            }

            if (enemy.GetComponent<enemyAtt>().ratioHealth() <= 0)
            {
                enemy.GetComponent<enemyAtt>().setLifeStats(true);
          
                _pcollider.disableAttacking();
                
                enemySpawn._enemySpawn.addToDeathList(enemy.gameObject);
                enemySpawn._enemySpawn.setRespawnCount();

                _levelPerkObject.increaseXp();

                perkComboTime = 3f;
                _levelPerkObject.multiplierKS++;
                isComboPerk = true;
                _pFollow.selectedObj = null;
                _pFollow._target = target.none;
            }
        }
      
    }
    
    public void skillArea1(bool isStunning = false, float area = 0.5f,float damage = 0)
    {
        Collider2D[] areaHitEnemies1 = Physics2D.OverlapCircleAll(transform.position, area, enemyLayer);

        foreach (var enemy in areaHitEnemies1)
        {
            _levelPerkObject.increasePerk();
            enemy.GetComponent<enemyAtt>().damageInput(damage);

            if (isStunning)
            {
                enemy.GetComponent<enemyAtt>().setStunEnemy(stunDuration);

            }

            if (enemy.GetComponent<enemyAtt>().ratioHealth() <= 0)
            {
                enemy.GetComponent<enemyAtt>().setLifeStats(true);
                _levelPerkObject.increaseXp();
                _pcollider.disableAttacking();
                _pcollider.targetInRange = false;
                enemySpawn._enemySpawn.addToDeathList(enemy.gameObject);
                enemySpawn._enemySpawn.setRespawnCount();

                _levelPerkObject.increaseXp();

                perkComboTime = 3f;
                _levelPerkObject.multiplierKS++;
                isComboPerk = true;
                _pFollow.selectedObj = null;
                _pFollow._target = target.none;
            }
        }

    }

    void OnDrawGizmosSelected()
    {
        #region Area range gizmos
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, areaRange);
        #endregion
        #region weapon range gizmos
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
        #endregion

    }


}
