﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerInventory : MonoBehaviour
{
    public inventoryObject[] _inventory;

    public void addItem(itemsObject item,int amount)
    {
        
        _inventory[(int)item.type].addItem(item, amount);
        if (item.type == itemType.potions)
        {
            playerUIManager._pUIManager.updatePotion(item);
        }
    }
}
