﻿using SAP2D;
using UnityEngine;

public class playerMoveAnimationManager : MonoBehaviour
{
    public playerCollider _pColl;
    public SAP2DAgent _agent;
     public void endBasicAttack()
    {
        
       //continue attack
        if (_agent._State == State.skill && !_pColl._statusEffect.isNoInterupt)
        {
          
            _pColl.setIsSkill(false);
        }
        else
        {
            if (!_pColl._statusEffect.isNoInterupt)
            {
                _agent._State = State.attacking;
            }
           
        }
              
        gameObject.SetActive(false);
       
    }
}
