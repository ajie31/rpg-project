﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Polarith.AI.Move;
using SAP2D;

public class playerCollider : MonoBehaviour
{
    // private AIMStabilization _stabilization;
    [Header("General properties")]
    #region General
    public playerStatusOBJ _pStatus;
    public statusEffect _statusEffect;
    private playerInventory _inventory;
    #endregion

    [Header("Player Navigation")]
    #region Navigation
    public SAP2DAgent _agent;
    private playerFollow _playerF;
    #endregion

    [Header("player combat related")]
    #region Combat
    public combatScript _combat;
    public bool targetInRange;
    [SerializeField] GameObject weapon;
    public Animator _attacBasic;
    public float attackDelay = 0.3f;
    private float attkDelayCount;
    #endregion



    private void Start()
    {
        _playerF = GetComponent<playerFollow>();
        _agent = GetComponent<SAP2DAgent>();
        _inventory = GetComponent<playerInventory>();
        _attacBasic.SetFloat("basicAttack", _pStatus.getAttribute[attributes.AttackSpd].totalValue);
        attkDelayCount = attackDelay;
    }
    private void Update()
    {
        #region if target selected
        if (_playerF.selectedObj != null && !_statusEffect.isNoInterupt)
        {
            #region pickup item
            if (_playerF._target == target.item  && Vector2.Distance(transform.position, _playerF.selectedObj.transform.position) <= .5f )
            {
                _inventory.addItem( _playerF._item._item, 1);
                _playerF.selectedObj.gameObject.SetActive(false);
                _playerF._target = target.none;
                _playerF.selectedObj = null;
            }

            #endregion

            #region detect enemy
            if ( _playerF._target == target.enemy &&
                (!targetInRange || (_agent._State == State.moving || _agent._State == State.idle))  
                && (!_statusEffect.isGotStun) && Vector2.Distance(transform.position, _playerF.selectedObj.transform.position) <= .9f)
            {
                _agent._State = State.attacking;
                 targetInRange = true;
                _agent.MovementSpeed = 0;
               // transform.right = (_playerF.selectedObj.transform.position - transform.position).normalized;
            }
            #endregion

            #region approaching enemy
            if (_playerF._target == target.enemy &&
                targetInRange  &&  _agent._State == State.attacking && (!_statusEffect.isGotStun
                && Vector2.Distance(transform.position, _playerF.selectedObj.transform.position) > .9f))
            {
                _agent._State = State.moving;              
                _agent.MovementSpeed = _pStatus.getAttribute[attributes.MovementSpd].actualValue;
                
            }
            #endregion
            /**if distance between target and player is too far
             */
            #region target out of sight
            if (_playerF.selectedObj != null && Vector2.Distance(transform.position, _playerF.selectedObj.transform.position) > 8f 
                && targetInRange && _playerF._target != target.enemy)
            {
                targetInRange = false;
               
                _playerF.selectedObj.GetComponent<enemyAtt>().setTerget(0);
                _playerF.selectedObj = null;             
            }

            #endregion

            #region attacking enemy

            if (_agent._State == State.attacking && targetInRange)
            {
                if (attkDelayCount > 0)
                {
                    attkDelayCount -= Time.deltaTime;
                }
                else
                {
                    weapon.SetActive(true);
                    _attacBasic.Play("normalAttack", -1, 0);
                    _attacBasic.SetFloat("basicAttack", _pStatus.getAttribute[attributes.AttackSpd].totalValue);
                    _agent._State = State.resting;
                    _combat.meleeAtt();
                    attkDelayCount = attackDelay;
                }

            }
            else if (!targetInRange || _playerF._target != target.enemy )
            {
                weapon.SetActive(false);
                attkDelayCount = attackDelay;
            }
            #endregion

        }


        #endregion
        #region imune physical damage BUFF!
        if (_statusEffect.DurationImunePhys > 0)
        {
            _statusEffect.DurationImunePhys -= Time.deltaTime;
        }
        else if (_statusEffect.isImunePhys && _statusEffect.DurationImunePhys <= 0)
        {
            _statusEffect.isImunePhys = false;
        }
        #endregion
    }


 

    /**whem player perform skill, some movement set in limitation (basic attack stop, movement stopped)
     */
    #region Skill related
    public void setIsSkill(bool status)
    {
        if (status)
        {
            _agent._State = State.skill;
            weapon.SetActive(false);
        }
        if (!status)
        {
            _agent.MovementSpeed = _pStatus.getAttribute[attributes.MovementSpd].actualValue;
            targetInRange = false;
            attkDelayCount = attackDelay;
            _agent._State = State.moving;//targetInRange = false;
        }
    }

    public void attack(bool isMultiple = false, bool isKnocking = false, bool isStunning = false,
                     bool isSlowDebuff = false, bool isSilence = false, bool isInstant = false)
    {

        // yield return _second1;

        weapon.SetActive(true);
        //  weaponColl.enabled = true;
        _attacBasic.Play("normalAttack", 0, 0);

        _attacBasic.SetFloat("basicAttack", _pStatus.getAttribute[attributes.AttackSpd].totalValue);

        _combat.skilledMelee(isMultiple, isKnocking, isStunning, isSlowDebuff, isSilence, isInstant);
    }
    #endregion

    public void disableAttacking()
    {
        weapon.SetActive(false);
        //targetInRange = false;
    }
}

