﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using SAP2D;
public class playerFollow : MonoBehaviour
{
    [Header("Player General Properties")]
    #region General
    public statusEffect _statusEffect;
    public playerStatusOBJ _pStatus;
    [SerializeField] private GameObject playerDestination;
    [SerializeField] private playerCollider _pCollider;
    [SerializeField] private LayerMask layers;
    [SerializeField] private combatScript _combat;

    #endregion

    #region selected Item
    [HideInInspector] public GameObject selectedObj;
    [HideInInspector] public item _item;
    public target _target;
    #endregion

    [Header("Player Navigation")]
    #region touch navigation
    [SerializeField] private Camera mCam;
    private Vector3 pointedPosition;
    private Collider2D touched;
    private Touch touch;
    private float distanceToScreen;
    [SerializeField] private SAP2DAgent _agent;
    #endregion

    [Header("Move Animation")]
    #region Animation property
    private Vector3 direction;
    private string dir, isMov, setup;
    [SerializeField] private Animator _pAnim;
    #endregion
    private bool touchOnce;

    void Start()
    {
        setup = "setup";
        isMov = "isMoving";
        dir = "direction";
        _pStatus.resetActual(attributes.MovementSpd);
        pointedPosition = Vector3.zero;
        // _agent = GetComponent<SAP2DAgent>();
        _pAnim.SetFloat(setup, 1);
        distanceToScreen = mCam.WorldToScreenPoint(gameObject.transform.position).z;
    }
    private void Update()
    {
       
        #region animation controll
        if (_agent._State == State.moving)
        {
            _pAnim.SetBool(isMov, true);
            direction = _agent.getDirection();
            if (direction.x > 0)
            {

                if (direction.y > 0.1)
                {
                    _pAnim.SetInteger(dir, 1);
                }
                else if (direction.y < -0.1)
                {
                    _pAnim.SetInteger(dir, 3);
                }
                else
                {
                    _pAnim.SetInteger(dir, 4);
                }
            }
            else if (direction.x < 0)
            {

                if (direction.y > 0.1)
                {
                    _pAnim.SetInteger(dir, 1);
                }
                else if (direction.y < -0.1)
                {
                    _pAnim.SetInteger(dir, 3);
                }
                else
                {
                    _pAnim.SetInteger(dir, 2);
                }

            }
            else if (direction.y > 0)
            {

                if (direction.y > 0.1)
                {
                    _pAnim.SetInteger(dir, 4);
                }
                else if (direction.y < -0.1)
                {
                    _pAnim.SetInteger(dir, 2);
                }
                else
                {
                    _pAnim.SetInteger(dir, 1);
                }
            }
            else if (direction.y < 0)
            {

                if (direction.x > 0.1)
                {
                    _pAnim.SetInteger(dir, 4);
                }
                else if (direction.x < -0.1)
                {
                    _pAnim.SetInteger(dir, 2);
                }
                else
                {
                    _pAnim.SetInteger(dir, 3);
                }
            }

        }
        else
        {
            _pAnim.SetBool(isMov, false);
            _pAnim.SetInteger(dir, 0);
            //  _pAnim.SetFloat(setup, 1);
        }

        #endregion



        #region attacking maneuver
        if (_target == target.enemy)
        {

            if (_agent._State == State.attacking || _agent._State == State.skill)
            {
                transform.right = (selectedObj.transform.position - transform.position).normalized;
            }
            else
            {
                playerDestination.transform.position = selectedObj.transform.position;
            }
        }
        #endregion

        #region got stun
        if (_statusEffect.gotStunDuration > 0)
        {
            _statusEffect.gotStunDuration -= Time.deltaTime;
        }
        else if (_statusEffect.isGotStun && _statusEffect.gotStunDuration <= 0)
        {
            _statusEffect.isGotStun = false;
            _statusEffect.gotStunDuration = 0;
            _statusEffect.setMovementSpeed(_pStatus.getAttribute[attributes.MovementSpd].totalValue, 0);
        }
        #endregion

        #region attack speed effect
        if (_statusEffect.ASPEffectDuration > 0)
        {
            _statusEffect.ASPEffectDuration -= Time.deltaTime;
        }
        else if (_statusEffect.isASPEffect && _statusEffect.ASPEffectDuration <= 0)
        {
            _statusEffect.resetAttackSpeed();
            _statusEffect.isASPEffect = false;
        }
        #endregion
        #region Movement Speed Effect
        if (_statusEffect.MSPEffectDuration > 0)
        {
            _statusEffect.MSPEffectDuration -= Time.deltaTime;
        }
        else if (_statusEffect.isMSPEffect && _statusEffect.MSPEffectDuration <= 0)
        {
            _statusEffect.resetMovementSpeed();
            _statusEffect.isMSPEffect = false;
        }
        #endregion

        #region reflect damage

        if (_statusEffect.reflectDamageDuration > 0)
        {
            _statusEffect.reflectDamageDuration -= Time.deltaTime;
        }
        else if (_statusEffect.isReflectDamage && _statusEffect.reflectDamageDuration <= 0)
        {           
            _statusEffect.isReflectDamage = false;
        }

        #endregion
    }

    private void FixedUpdate()
    {
        /**player touch detect object, if touch enemy it become target, if touch item it will pickup item, 
         * if walkable player will move, if NPC it will interact, if not listed it will do nothing
                      */
        if (Input.touchCount > 0 && !touchOnce && !_statusEffect.isNoInterupt)
        {

            touch = Input.GetTouch(0);
            pointedPosition = Camera.main.ScreenToWorldPoint(new Vector3(touch.position.x,
                    touch.position.y, distanceToScreen));

            touched = Physics2D.OverlapCircle(pointedPosition, .1f, layers);

            if (touched != null && !_statusEffect.isNoInterupt && !_statusEffect.isGotStun)
            {
                if (!IsPointerOverUIObject()) /** make touch able to detect UI layer */
                {
                    touchOnce = true;

                    #region touch walkable
                    if (touched.gameObject.CompareTag("walkable"))
                    {
                        _agent._State = State.moving;
                        _target = target.none;
                        
                        _agent.MovementSpeed = _pStatus.getAttribute[attributes.MovementSpd].actualValue;
                        playerDestination.transform.position = pointedPosition;
                        playerDestination.SetActive(true);
                        playerDestination.transform.parent = null;
                        _pCollider.disableAttacking();

                    }
                    #endregion

                    #region touch enemy
                    if (touched.gameObject.CompareTag("enemy"))
                    {
                        _agent._State = State.moving;
                        if (selectedObj != null && selectedObj.CompareTag("enemy"))
                        {

                            selectedObj.GetComponent<enemyAtt>().setTerget(0);
                        }

                        
                        _agent.MovementSpeed = _pStatus.getAttribute[attributes.MovementSpd].actualValue;
                        selectedObj = touched.gameObject;
                        selectedObj.GetComponent<enemyAtt>().setTerget(1);
                       // selectedObj.GetComponent<enemyAtt>().setKnocked(false);

                        _combat.setEnemyAttribute(selectedObj.GetComponent<enemyAtt>());
                        playerDestination.transform.position = touched.transform.position;
                        playerDestination.SetActive(false);
                        _target = target.enemy;
                    }
                    #endregion

                    #region touch Item
                    if (touched.gameObject.CompareTag("item"))
                    {
                        _agent._State = State.moving;
                        selectedObj = touched.gameObject;
                        _item = selectedObj.GetComponent<item>();
                        _target = target.item;
                        _agent.MovementSpeed = _pStatus.getAttribute[attributes.MovementSpd].actualValue;

                        playerDestination.transform.position = pointedPosition;
                        playerDestination.SetActive(true);
                    }
                    #endregion

                }
            }             
        }
        else if (touchOnce && Input.touchCount < 1)
        {
            touchOnce = false;           
        }


    }
    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }



    public void setDestination(Vector2 des)
    {
        playerDestination.transform.position = des;
    }
    public void setSpeed(float speed)
    {

        _agent.MovementSpeed = speed;
    }

}
public enum target
{
    none,
    enemy,
    item,
    npc
}