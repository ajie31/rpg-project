﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class playerWeapon : MonoBehaviour
{
    [SerializeField] playerCollider _pcollider;
    private enemyAtt _eAttr;
    private bool skill;
    private float multiplier;
    private float damageInstant;
    private float getDamage;
    private bool isInstant;

    private float timeDebuffSlow;
    private float slowAmount;

    public bool isColliding;

    private bool isSilence;
    private float silenceDuration;

  

  /*  private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("enemy") && !isColliding)
        {
           // isColliding = true;
           
            if (skill)
            {
                if (playerAttr._pAttribute.getCountTarget())
                {

                    if (playerAttr._pAttribute.getIsKnock())
                    {
                       // Debug.Log("knocking");
                        other.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;

                        other.GetComponent<Rigidbody2D>().AddForce((other.transform.position - transform.position) * 200, ForceMode2D.Force);
                        other.GetComponent<enemyCollider>().setGotKnocked(true);
                        //knockTime = .5f;
                    }
                    if (playerAttr._pAttribute.getIsStun())
                    {
                        other.GetComponent<enemyAtt>().setStunEnemy(playerAttr._pAttribute.getStunDuration());
                    }

                    if (playerAttr._pAttribute.getSlowDebuffModifier())
                    {
                        other.GetComponent<enemyAtt>().activateSlowDebuff(timeDebuffSlow, slowAmount);
                        playerAttr._pAttribute.setSlowDebuffModifier(false);
                    }


                    if (isSilence)
                    {
                        other.GetComponent<enemyAtt>().setSilent(true, silenceDuration);
                    }


                    if (isInstant)
                    {
                        other.GetComponent<enemyAtt>().damageInput(playerAttr._pAttribute.getDamage(1, false,true, damageInstant));
                    }
                    else
                    {
                        other.GetComponent<enemyAtt>().damageInput(playerAttr._pAttribute.getDamage(multiplier, true));
                    }

                    if (other.GetComponent<enemyAtt>()._profile.getRatioAttribute(attributes.Health) <= 0)
                    {
                        other.GetComponent<enemyAtt>().setLifeStats(true);
                        _pcollider.disableAttacking();
                        other.GetComponent<enemyAtt>().getEnemyAI().CanSearch = false;
                        enemySpawn._enemySpawn.addToDeathList(other.gameObject);
                        enemySpawn._enemySpawn.setRespawnCount();
                        // gotAttacked = false;
                        if (playerAttr._pAttribute.getHealthSteal())
                        {
                            playerAttr._pAttribute.heal(0, true);
                           // playerAttr._pAttribute.setHealthSteal(false, 0);
                        }
                        other.GetComponent<enemyAtt>().gameObject.SetActive(false);

                    }
                }
                else
                {

                    if (playerAttr._pAttribute.getIsKnock())
                    {

                        _eAttr.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;

                        _eAttr.GetComponent<Rigidbody2D>().AddForce((other.transform.position - transform.position) * 200, ForceMode2D.Force);
                        _eAttr.GetComponent<enemyCollider>().setGotKnocked(true);
                        //knockTime = .5f;
                    }
                    if (playerAttr._pAttribute.getIsStun())
                    {
                        _eAttr.setStunEnemy(playerAttr._pAttribute.getStunDuration());
                    }

                    if (playerAttr._pAttribute.getSlowDebuffModifier())
                    {
                     
                        _eAttr.activateSlowDebuff(timeDebuffSlow,slowAmount);
                        playerAttr._pAttribute.setSlowDebuffModifier(false);
                    }
                    if (isSilence)
                    {
                        other.GetComponent<enemyAtt>().setSilent(true, silenceDuration);
                    }
                    if (isInstant)
                    {
                       _eAttr._profile.damageInput(playerAttr._pAttribute.getDamage(1, false, true, damageInstant));
                    }
                    else
                    {
                       _eAttr._profile.damageInput(playerAttr._pAttribute.getDamage(multiplier, true));
                    }

                    if (_eAttr._profile.getRatioAttribute(attributes.Health) <= 0)
                    {
                        _eAttr.setLifeStats(true);
                        _pcollider.disableAttacking();
                        _eAttr.getEnemyAI().CanSearch = false;
                        enemySpawn._enemySpawn.addToDeathList(_eAttr.gameObject);
                        enemySpawn._enemySpawn.setRespawnCount();
                        if (playerAttr._pAttribute.getHealthSteal())
                        {
                            playerAttr._pAttribute.heal(0, true);
                          //  playerAttr._pAttribute.setHealthSteal(false, 0);
                        }
                        // gotAttacked = false;
                        _eAttr.gameObject.SetActive(false);

                    }

                }

               
              
            }
            else
            {
               
               _eAttr._profile.damageInput(playerAttr._pAttribute.getDamage());
              //  _text.text = playerAttr._pAttribute.getDamage().ToString();
                if (_eAttr._profile.getRatioAttribute(attributes.Health) <= 0)
                {
                    _eAttr.setLifeStats(true);
                    _pcollider.disableAttacking();
                    _eAttr.getEnemyAI().CanSearch = false;
                    enemySpawn._enemySpawn.addToDeathList(_eAttr.gameObject);
                    enemySpawn._enemySpawn.setRespawnCount();
                   
                    if (playerAttr._pAttribute.getHealthSteal())
                    {
                        playerAttr._pAttribute.heal(0, true);
                       // playerAttr._pAttribute.setHealthSteal(false, 0);
                    }
                    // gotAttacked = false;
                    _eAttr.gameObject.SetActive(false);

                }
            }

           // StartCoroutine(textPosition(other.transform));
           
        }
    }

  /*  private IEnumerator textPosition( Transform target)
    {
        yield return null;
        yield return null;
        yield return null;
        _text.transform.position = target.position;
        _text.gameObject.SetActive(true);
    }
  */


}
