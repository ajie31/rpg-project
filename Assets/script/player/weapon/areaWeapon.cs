﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class areaWeapon : MonoBehaviour
{
    [SerializeField] playerCollider _pCollider;
    private bool isIncreaseArea;
    private float range;
    private float duration;
    private float damage;
    private Vector2 incrementRange = new Vector2(1,1);

    void Update()
    {
      
        if (duration>0)
        {
            duration -= Time.deltaTime;
        }
        else
        {
            duration = 0;
            gameObject.SetActive(false);
        }
    }

 /*   private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("enemy"))
        {
            collision.GetComponent<enemyAtt>().damageInput(damage);
            if (playerAttr._pAttribute.getIsKnock())
            {

                collision.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;

                collision.GetComponent<Rigidbody2D>().AddForce((collision.transform.position - transform.position) * 200, ForceMode2D.Force);
                collision.GetComponent<enemyCollider>().setGotKnocked(true);
                playerAttr._pAttribute.setKnock(false);
                //knockTime = .5f;
            }
            if (playerAttr._pAttribute.getIsStun())
            {
                collision.GetComponent<enemyAtt>().setStunEnemy(playerAttr._pAttribute.getStunDuration());
                playerAttr._pAttribute.setStun(false, 0);
            }

            if (collision.GetComponent<enemyAtt>()._profile.getRatioAttribute(attributes.Health) <= 0)
            {
                collision.GetComponent<enemyAtt>().setLifeStats(true);
                _pCollider.disableAttacking();
                collision.GetComponent<enemyAtt>().getEnemyAI().CanSearch = false;
                enemySpawn._enemySpawn.addToDeathList(collision.gameObject);
                enemySpawn._enemySpawn.setRespawnCount();
                // gotAttacked = false;
                collision.gameObject.SetActive(false);

            }
           
        }
    }*/

    public void setArea(bool isInc,float _dur,float _range,float _damage)
    {
        isIncreaseArea = isInc;
        duration = _dur;
        range = _range;
        if (_damage <=0 )
        {
            damage = 0;
        }
        else
        {
            damage = _damage;
        }
        transform.localScale = new Vector3(range, range,1);
    }
}
