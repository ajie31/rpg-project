﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class cameraManager : MonoBehaviour
{
    [SerializeField]private CinemachineVirtualCamera _virtuanCam;
     private CinemachineFramingTransposer _virtuanCamT;
    [SerializeField] private Transform _pTrans;

    private void Start()
    {
        _pTrans = GameObject.FindGameObjectWithTag("Player").transform;
        _virtuanCamT = _virtuanCam.GetCinemachineComponent<CinemachineFramingTransposer>();
    }

    public IEnumerator teleporting()
    {
        _virtuanCam.Follow = null;
     
        _virtuanCamT.m_LookaheadIgnoreY = false;
        yield return null;
        _virtuanCam.Follow = _pTrans;
        _virtuanCamT.m_LookaheadIgnoreY = true;
    }

}
