﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public enum castStatus
{
    normalState,
    noMana,
    noTarget,
    gotSilent,
    gotStun,
    isNotInterupt
}

public class skillMan : MonoBehaviour
{
    public static skillMan _skillsManager;
    public castStatus _castStatus;
    public statusEffect _statusEffect;

    public skillGroup[] _skills;   
    public Image[] skillBttnImages;
    private float skillCoolDown;

    [SerializeField] TextMeshProUGUI textSkillStatus;
   
    private float textappearDuration = 1.5f;

    private void Awake()
    {

        if (_skillsManager == null)
        {
            _skillsManager = this;
        }
    }
    private void Start()
    {     
        StartCoroutine(setButtonSprite());

    }
    private void Update()
    {
        if (_castStatus == castStatus.noMana && textappearDuration > 0)
        {
            textappearDuration -= Time.deltaTime;
            if (!textSkillStatus.gameObject.activeInHierarchy)
            {
                textSkillStatus.text = "No Mana!";
                textSkillStatus.gameObject.SetActive(true);
            }
        }
        if (_castStatus == castStatus.gotSilent && textappearDuration > 0)
        {
            textappearDuration -= Time.deltaTime;
            if (!textSkillStatus.gameObject.activeInHierarchy)
            {
                textSkillStatus.text = "Silent!";
                textSkillStatus.gameObject.SetActive(true);
            }
        }
        if ((_castStatus == castStatus.gotStun || _castStatus == castStatus.isNotInterupt) && textappearDuration > 0)
        {
            textappearDuration -= Time.deltaTime;
            if (!textSkillStatus.gameObject.activeInHierarchy)
            {
                textSkillStatus.text = "Can Not Cast Power";
                textSkillStatus.gameObject.SetActive(true);
            }
        }
        if (_castStatus == castStatus.noTarget && textappearDuration > 0)
        {
            textappearDuration -= Time.deltaTime;
            if (!textSkillStatus.gameObject.activeInHierarchy)
            {
                textSkillStatus.text = "No Target!";
                textSkillStatus.gameObject.SetActive(true);
            }
        }
        if (textappearDuration <= 0)
        {
            textappearDuration = 1.5f;
            _castStatus = castStatus.normalState;
            textSkillStatus.text = null;
            textSkillStatus.gameObject.SetActive(false);
        }
    }
  
    public IEnumerator setButtonSprite()
    {
        for (int i = 0; i < _skills[0]._skill.Length; i++)
        {
            skillBttnImages[i].sprite = _skills[0]._skill[i].artwork;
            yield return null;
        }
        
    }
    public float getCooldown()
    {

        return skillCoolDown; 

    }
    public void setCooldown(float val)
    {
        skillCoolDown = val;
    }

    public void getSkills(int i)
    {
        textSkillStatus.gameObject.SetActive(false);
        _castStatus = castStatus.normalState;
        textappearDuration = 1.5f;
        if (!_statusEffect.isSilent && !_statusEffect.isNoInterupt
             && !_statusEffect.isGotStun)
        {
            _skills[0]._skill[i].isMastery = false;

            _skills[0]._skill[i].skillInAction();
        }
        else
        {
            if (_statusEffect.isSilent)
            {
                _castStatus = castStatus.noMana;
            }
            else
            {
                _castStatus = castStatus.isNotInterupt;
             
            }
            skillCoolDown = 0;
     

        }


    }

    /*public void skillEdit(int chosenInd,int fromList,bool isAdd = false )
    {
        if (isAdd)
        {

            limitChosen++;
        }
        else
        {
            choosenSkill[chosenInd] = null;
            limitChosen--;
        }
    }
    */

}
