﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemySpawn : MonoBehaviour
{
    public static enemySpawn _enemySpawn;
    [SerializeField] Vector2[] spawnPosition;
    [SerializeField] GameObject _enemy;
    [SerializeField] int countEnemy;
    private int countRespawn;
    private GameObject[] enemiesPool;
    private GameObject spawnedEnemy;
    private WaitForSeconds _secondDelay;
    private WaitForSeconds _secondDelayRespawn;
    private bool respawnStatus;
    private List<GameObject> deathList;

    private void Awake()
    {
        if (_enemySpawn == null)
        {
            _enemySpawn = this;
        }
    }

    private void Start()
    {
        _secondDelay = new WaitForSeconds(.2f);
        _secondDelayRespawn = new WaitForSeconds(10f);
        enemiesPool =  new GameObject[countEnemy];
        deathList = new List<GameObject>();
        for (int i = 0; i < countEnemy; i++)
        {
            GameObject enemy = (GameObject)Instantiate(_enemy);
            enemy.SetActive(false);
            enemy.GetComponent<enemyAtt>().setPosition(spawnPosition[i]);       
            enemiesPool[i] = enemy;

        }
        StartCoroutine(spawnInTime());
    }
    private void Update()
    {
        if (countRespawn > 0 && !respawnStatus)
        {
            respawnStatus = true;
            StartCoroutine(respawn());
        }
    }

    public GameObject getPooledEnemies()
    {
        for (int i = 0; i < enemiesPool.Length; i++)
        {
            if (!enemiesPool[i].activeInHierarchy)
            {
                return enemiesPool[i];
            }
        }
        return null;
    }

    public void spawnEnemy()
    {
        spawnedEnemy = getPooledEnemies();
        spawnedEnemy.transform.position = spawnedEnemy.GetComponent<enemyAtt>().getPosition();
        spawnedEnemy.GetComponent<enemyAtt>().resetStats();
        spawnedEnemy.SetActive(true);

        if (respawnStatus)
        {
            spawnedEnemy.GetComponent<enemyCollider>().resetStatsCollider();
        }      
       
    }

    public void respawning()
    {
        spawnedEnemy = deathList[0];
        spawnedEnemy.transform.position = spawnedEnemy.GetComponent<enemyAtt>().getPosition();
        spawnedEnemy.GetComponent<enemyAtt>().resetStats();
        spawnedEnemy.SetActive(true);
        if (respawnStatus)
        {
            spawnedEnemy.GetComponent<enemyCollider>().resetStatsCollider();
        }
    }

    public IEnumerator spawnInTime()
    {
        yield return _secondDelay;
        for (int i = 0; i < countEnemy; i++)
        {

            
            yield return null;
            spawnEnemy();
        }

    }

    public void setRespawnCount()
    {
        countRespawn += 1;
    }

    public IEnumerator respawn()
    {
        while (true)
        {
            if (countRespawn==0)
            {
                respawnStatus = false;
                break;
            }
            yield return _secondDelayRespawn;
            respawning();
            deathList.RemoveAt(0);
            countRespawn -= 1;
        }
    }
    public void addToDeathList(GameObject go)
    {

        deathList.Add(go);
    }


        
}
