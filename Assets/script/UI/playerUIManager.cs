﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


public class playerUIManager : MonoBehaviour
{
    [Header("general property")]
    public static playerUIManager _pUIManager;
    public healingSystem _heal;
    public levelingAndPerk _levelPerkObject;
    public playerStatusOBJ _pStatus;
    public playerWeaponManager _pWeaponM;

    #region all HUD metters Property
    [Header("HP HUD property")]
    [SerializeField] private Image _HPFill;
    [SerializeField] private TextMeshProUGUI _HPStatusTextr;
    [Header("Mana HUD Property")]
    [SerializeField] private Image _ManaFill;
    [SerializeField] private TextMeshProUGUI manaStatusText;
    [Header("Focus HUD Property")]
    [SerializeField] private Image _focusFill;
    [SerializeField] private Image _focusFill1;
    [Header("XP HUD Property")]
    [SerializeField] private Image _XpFill;
    [SerializeField] private TextMeshProUGUI levelText;
    [Header("Perk Indicator Property")]
    [SerializeField] private Image _perkMeter;
    [SerializeField] private TextMeshProUGUI perkLevelText;

    #endregion

    [Header("Mastery HUD")]
    [SerializeField] comboButton _masteryBtt;
    private bool focusCheckonce;

    [Header("Heals HUD Property")]
    #region Heals property
    [SerializeField] private TextMeshProUGUI hpPotionText;
    public Image healthPotion;
    [SerializeField] private TextMeshProUGUI manaPotionText;
    public Image manaPotion;
    [SerializeField] private inventoryObject playerInventory;
    private float delayHealNumber = 4;
    private float delayRestoreNumber = 4;
    private bool healCd, manaCd;
    private int amountHeal, amountMana;

    #endregion

    [Header("Weapon Swap Property")]
    [SerializeField] private Sprite[] _weaponIcons;
    [SerializeField] private Image weapinicon;

    private void Awake()
    {
        if (_pUIManager == null)
        {
            _pUIManager = this;
        }
        _pStatus.setupDictionary();
    }
    private void Start()
    {
       
        _pStatus.bonusStrArmor();
        updateHPVal();
        updateManaVal();
        updateFocusVal();
        updateXpMeter();
        updatePlayerLevel();
        updatePerkMeter();
        potionStartCheck();
        weaponAtStart();
        
      
    }
    private void Update()
    {
        if (delayHealNumber < 4)
        {
            delayHealNumber += Time.deltaTime;
            healthPotion.fillAmount = delayHealNumber/4;
            // delayText.text = "healing";

        }
        else if (delayHealNumber  >= 4 && healCd)
        {
            healCd = false;
            healthPotion.fillAmount = 1;
        }

        if (delayRestoreNumber < 4 )
        {
            delayRestoreNumber += Time.deltaTime;
            // delayTextM.text = "Mana";
            manaPotion.fillAmount = delayRestoreNumber/4;

        }
        else if (delayRestoreNumber  >= 4 && manaCd)
        {
            manaCd = false;
            manaPotion.fillAmount = 1;
        }
    }

    #region Health Related
    public void updateHPVal()
    {
        if (_pStatus.percentageValue(attributes.Health) >= 0)
        {
            // _HPMetter.value = playerAttr._pAttribute._profile.getRatioAttribute(attributes.Health);
            _HPFill.fillAmount = _pStatus.percentageValue(attributes.Health);
            _HPStatusTextr.text = _pStatus.getAttribute[attributes.Health].actualValue.ToString("0");
        }     
    }
    public void updateHpPotion(int amount)
    {
        amountHeal = amount;
        hpPotionText.text = amountHeal.ToString();
    }
    public void healing()
    {
        if (delayHealNumber / 4 >= 1 && _pStatus.percentageValue(attributes.Health) < 1 && amountHeal > 0)
        {
            _heal.heal(30);
            healCd = true;
            delayHealNumber = 0;
            playerInventory.decreaseItem(0, 1);
        }

    }
    #endregion

    #region Mana Related
    public void updateManaVal()
    {

        if (_pStatus.percentageValue(attributes.ManaPoint) >=0)//playerAttr._pAttribute.getManaPoint() >= 0)
        {
            // _manaMeter.value = playerAttr._pAttribute.getManaPoint();
            _ManaFill.fillAmount = _pStatus.percentageValue(attributes.ManaPoint);
            manaStatusText.text = _pStatus.getAttribute[attributes.ManaPoint].actualValue.ToString("0");
        }
    }
    public void updateManaPotion(int amount)
    {
        amountMana = amount;
        manaPotionText.text = amountMana.ToString();
    }

    public void manaRestore()
    {
        if (delayRestoreNumber / 4 >= 1 && _pStatus.percentageValue(attributes.ManaPoint) < 1 && amountMana > 0)
        {
            _heal.manaIncrease(20);
            manaCd = true;
            delayRestoreNumber = 0;
            playerInventory.decreaseItem(1, 1);
            //updateManaVal();
            // StartCoroutine(delayRestore());
        }
    }
    #endregion

    #region Player Focus point
    public void updateFocusVal()
    {
        if (_pStatus.getFocusRatio() >= 0)
        {
            _focusFill.fillAmount = _pStatus.getFocusRatio();
            _focusFill1.fillAmount = _pStatus.getFocusRatio();

            if (_pStatus.getFocusRatio() >= 1 && !focusCheckonce)
            {
                focusCheckonce = true;
                _masteryBtt._comboBtt.interactable = true;
                _masteryBtt._comboBttAnim.SetBool("isSSReady", true);
            }
            else if (_pStatus.getFocusRatio() < 1 && focusCheckonce)
            {
                focusCheckonce = false;
            }

        }

    }
    #endregion

    #region player Status Info
    public void updateXpMeter()
    {
        //_XpFill.fillAmount = playerAttr._pAttribute.getExp(false);
        _XpFill.fillAmount = _pStatus.getExpPercent();
    }
    public void updatePlayerLevel()
    {
        levelText.text = _pStatus.getPlayerLevel().ToString();//playerAttr._pAttribute.getPlayerLevel().ToString();
    }
    public void updatePerkMeter()
    {
        _perkMeter.fillAmount = _pStatus.getPerkPercent();
    }
    public void updatePerkLvl(int perk)
    {
        perkLevelText.text = perk.ToString();
    }

    #endregion

    #region inventoryPotion related
    public void potionStartCheck()
    {
        for (int i = 0; i < playerInventory.container.Count; i++)
        {
            if (playerInventory.database.getId[playerInventory.container[i].item] == 0 || playerInventory.database.getId[playerInventory.container[i].item] == 1)
            {
                updatePotion(playerInventory.container[i].item);
            }
        }
        
    }

    public void updatePotion(itemsObject _item)
    {
        if (playerInventory.database.getId[_item] == 0)
        {
            updateHpPotion(playerInventory.getItemAmount(_item));
        }
        else if (playerInventory.database.getId[_item] == 1)
        {
            updateManaPotion(playerInventory.getItemAmount(_item));
        }
        else
        {
            updateManaPotion(0);
            updateHpPotion(0);
        }
    }
    #endregion
    #region weapon switch
    public void switchWeapon(bool status)
    {
        _pWeaponM.changeWeapon(status);
        weapinicon.sprite = _weaponIcons[(int)_pWeaponM._weaponChosen[0]._weaponClass];
        _pStatus.setMaxPerk();
        updatePerkLvl(_pStatus._playerPerk[(int)_pStatus._weaponClass]._perkLevel);
        updatePerkMeter();
    }

    private void weaponAtStart()
    {
        _pWeaponM.weaponSetup();
        weapinicon.sprite = _weaponIcons[(int)_pWeaponM._weaponChosen[0]._weaponClass];
        _pStatus.setMaxPerk();
        updatePerkLvl(_pStatus._playerPerk[(int)_pStatus._weaponClass]._perkLevel);
        updatePerkMeter();
    }
    #endregion

}
