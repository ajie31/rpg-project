﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class circlePool : MonoBehaviour
{
    public static circlePool _circlePool;
    [SerializeField] private GameObject theCircle;
    [SerializeField] private Transform parent;
    private Vector2[] pattern;

    private GameObject[] circleCombo;
    private List<GameObject> activeList;

    public int circleAmount;

    private GameObject circleReady;
    private bool isDrawingInterupt;

    private void Awake()
    {
        if (_circlePool == null)
        {
            _circlePool = this;
        }
    }

    void Start()
    {
        
        activeList = new List<GameObject>();
        circleCombo = new GameObject[circleAmount];

        for (int i = 0; i < circleAmount; i++)
        {
            GameObject circle = Instantiate(theCircle);
            circleCombo[i] = circle;
            circle.SetActive(false);
            circle.transform.SetParent(parent);
            circle.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    #region list of active circle
    public void resetActiveCircleList()
    {
        activeList.Clear();
    }
    #endregion

    /**circle got pooled at start method and called at drawPatter() method
     * 
     * 
    */
    #region get circle
    private GameObject getCircle()
    {
        for (int i = 0; i < circleCombo.Length; i++)
        {
            if (!circleCombo[i].activeInHierarchy)
            {
                activeList.Add(circleCombo[i]);
                circleCombo[i].transform.SetAsFirstSibling();
                return circleCombo[i];

            }
        }
        return null;
    }
    #endregion

    /**drawing pattern is called at comboButton script to activate combo*/
     
    #region draw circle when activate Combo

    public void startDarwCircle(Vector2[] _pattern)
    {
        pattern = _pattern;
        StartCoroutine(drawPattern());
    }
    public IEnumerator drawPattern()
    {
        
       // succcessCircle = 0;
        int j = 0;
        while (true)
        {
            if (pattern != null)
            {
                circleReady = getCircle();
                circleReady.SetActive(true);

                setRectPossition(circleReady, pattern[j]);
                j++;
                if (j >= pattern.Length || isDrawingInterupt)
                {
                    break;
                }
            }

            yield return null;
        }

    }

    private void setRectPossition(GameObject circle,Vector2 post)//GameObject circle, int ind, int ind1)
    {
        circle.GetComponent<RectTransform>().anchorMax = post;//skillMan._skillsManager.getPattern(ind, ind1);//positionCircleMax[ind];
        circle.GetComponent<RectTransform>().anchorMin = post;// positionCircleMin[ind];
        circle.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        circle.GetComponent<circleScript>().startLerping();

    }
    #endregion

 
    public void disabledCombo()
    {
        isDrawingInterupt = false;
        foreach (var circle in activeList)
        {
            circle.SetActive(false);
        }
        activeList.Clear();
        parent.gameObject.SetActive(false);
    }


    /**if combo session is done by checking circle combo array length in skillObject, and continue to next session
     * if combo is last combo by checking limitSkill in skill manager combo wasDone set true and combo is fully finished
     * activeList is cleared
     * invoke activateCombo() from comboButton to disable combo
     */

}
