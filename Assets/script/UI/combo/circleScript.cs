﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class circleScript : MonoBehaviour, IPointerEnterHandler
{
    [SerializeField] circleComboObj _cObj;
    [SerializeField] private Vector2 startPost;
    [SerializeField] private Vector2 target;
    [SerializeField] private Vector2 targetCircleTouched;
    private Vector2 missEd = Vector2.zero;
    [SerializeField] private float startVal;
    [SerializeField] private float endVal;

    [SerializeField] private CanvasGroup cgCricle;
    [SerializeField] private RectTransform theCircle;
    [SerializeField] private RectTransform ringCircle;

    private TextMeshProUGUI _text;

    [SerializeField] private float lerpVelocity;

    private bool isLerping;
    private bool isSetTouch;
    private bool isComboTime;
    private bool isMissing;
    private bool isTouched;

    private float startTimeLerp;
    private float timeSiceStarted;
    private float percentage;
    private float alphaTouched;
   [SerializeField] private RectTransform positionC;

    public void startLerping()
    {
       
        isMissing = false;
        theCircle.localScale = target;
        cgCricle.alpha = 0;
        startTimeLerp = Time.time;
        timeSiceStarted = 0;
        percentage = 0;
        isSetTouch = false;
        isLerping = true;
    }

    public void startMissing()
    {
       
        startTimeLerp = Time.time;
        isComboTime = false;
        timeSiceStarted = 0;
        percentage = 0;
        isSetTouch = false;
        isMissing = true;
        _text = textPooling._textPooling.getText(false, positionC.anchorMin);
        _text.gameObject.SetActive(true);

    }

    public void comboTouched()
    {
        isLerping = false;
        startTimeLerp = Time.time;
        timeSiceStarted = 0;
        percentage = 0;
        alphaTouched = cgCricle.alpha;
        isTouched = true;
    }

    public void OnPointerEnter(PointerEventData pointerData)
    {
        if (isComboTime)
        {
            _cObj.increaseSuccess();
            _text = textPooling._textPooling.getText(true, positionC.anchorMin);
            _text.gameObject.SetActive(true);         
            isComboTime = false;

            comboTouched();
        }     
    }

    private void FixedUpdate()
    {
        if (isLerping)
        {
            timeSiceStarted = Time.time - startTimeLerp;
            percentage = timeSiceStarted / (lerpVelocity/4);

            ringCircle.localScale = Vector2.Lerp(startPost, target, percentage);
            cgCricle.alpha = Mathf.Lerp(startVal, endVal, percentage);
            if (percentage >=.25f && !isSetTouch)
            {
                isSetTouch = true;
                isComboTime = true;
            }

            if ((percentage/3) >= 1.0f || isTouched)
            {
               
                isLerping = false;
                startMissing();
            }
        }
        if (isTouched)
        {
            timeSiceStarted = Time.time - startTimeLerp;
            percentage = timeSiceStarted / (lerpVelocity /1.5f);

            theCircle.localScale = Vector2.Lerp(target, targetCircleTouched, percentage);
            cgCricle.alpha = Mathf.Lerp(alphaTouched, 0, percentage);

            if (percentage >= 1f)
            {
               
                isTouched = false;
                gameObject.SetActive(false);
            }

        }

        if (isMissing)
        {
            timeSiceStarted = Time.time - startTimeLerp;
            percentage = timeSiceStarted / (lerpVelocity/5);

            theCircle.localScale = Vector2.Lerp(target, missEd, percentage);
            cgCricle.alpha = Mathf.Lerp(endVal, startVal, percentage);

            if (percentage >= 1.0f)
            {
                _cObj.increaseFail();
                isMissing = false;
                gameObject.SetActive(false);
            }
        }

      

    }

}
