﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class comboButton : MonoBehaviour
{
    public masteryManager _mM;

   [SerializeField] private GameObject overlayCombo;
    private WaitForSeconds _seconds;
    public Animator _comboBttAnim;
    // Start is called before the first frame update
    private TextMeshProUGUI _bttnText;
    private bool comboIsActive;
    public Button _comboBtt;
   

    private void Start()
    {
        _seconds = new WaitForSeconds(.3f);
        _bttnText = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
    }
    /*
    private void Update()
    {
        if (countCoolDown > 0)
        {
            countCoolDown -= Time.deltaTime;
            _bttnText.text = ((int)countCoolDown).ToString();
        }
        else if(countCoolDown <=0 )
        {
          
             playerAttr._pAttribute.setSSReady(true); 
            _bttnText.text = "SS";
        }
    }
*/
    public void activateCombo()
    {
        if (_mM.isMasteryActive)
        {             
            comboIsActive = false;
            _mM.interuptMastery();
            _comboBtt.interactable = false;
            _comboBttAnim.SetBool("isSSReady", false);
            StartCoroutine(disableOverlay());
        }
        else
        {
            if (_mM._pStatus.getFocusRatio() >= 1)
            {

            }
            overlayCombo.SetActive(true);
            _mM.activateMasery();
            _comboBttAnim.SetBool("isSSReady", false);
            playerUIManager._pUIManager.updateFocusVal();
        }
        
    }

    private IEnumerator disableOverlay()
    {
        yield return _seconds;
        overlayCombo.SetActive(false);
       // playerAttr._pAttribute.setSSdamageF();
       // circlePool._circlePool.setComboSccsTozero();
    }

   
}
