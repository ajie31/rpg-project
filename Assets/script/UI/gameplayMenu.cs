﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class gameplayMenu : MonoBehaviour
{
    public levelingAndPerk _levelPerkObject;
    [Header("Display Properties")]
    public GameObject HUD;
    public GameObject gameplayMenuPanel;
    [Header("Inside Content ")]
    public GameObject[] contentMenu;
    public GameObject[] subMenu;

    [Header("Header Main Menu ")]
    public string[] menuTitles;
    public Sprite[] menuIcons;   
    public Image MenuIcon;  
    private int menuPointer = 1;
    public TextMeshProUGUI menuTitleText;
    public TextMeshProUGUI nextMenuText;
    public TextMeshProUGUI prevMenuText;
    public TextMeshProUGUI skillPointText;
    public GameObject nextButton;
    public GameObject prevButton;

    private bool isGamePaused;
    //skill section
    private void Start()
    {      
        setSPText();
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Escape) && !isGamePaused )
        {
            isGamePaused = true;
            HUD.SetActive(false);
            gameplayMenuPanel.SetActive(true);
            setSPText();

        }
        else if (Input.GetKeyDown(KeyCode.Escape) && isGamePaused )
        {
            isGamePaused = false;
            HUD.SetActive(true);
            gameplayMenuPanel.SetActive(false);

        }
    }
  
    public void setSPText()
    {
        skillPointText.text = _levelPerkObject._pStatus.playerSkillPt.ToString();
    }

    public void nextmenu()
    {
        if (menuPointer < 3)
        {
            menuPointer += 1;
            contentMenu[menuPointer - 1].SetActive(false);
            contentMenu[menuPointer].SetActive(true);
            MenuIcon.sprite = menuIcons[menuPointer];
            menuTitleText.text = menuTitles[menuPointer];

            if (menuPointer<3)
            {
                nextMenuText.text = menuTitles[menuPointer + 1];
            }
           
            prevMenuText.text = menuTitles[menuPointer - 1];
            subMenu[menuPointer - 1].SetActive(false);
            subMenu[menuPointer].SetActive(true);
            if (menuPointer == 1)
            {
                prevButton.SetActive(true);
            }
            if (menuPointer == 3)
            {
                nextButton.SetActive(false);
            }
        }
    }

    public void prevMenu()
    {
        if (menuPointer > 0)
        {
            menuPointer -= 1;
            contentMenu[menuPointer + 1].SetActive(false);
            contentMenu[menuPointer].SetActive(true);
            MenuIcon.sprite = menuIcons[menuPointer];
            menuTitleText.text = menuTitles[menuPointer];
            nextMenuText.text = menuTitles[menuPointer + 1];
            if (menuPointer > 0)
            {
                prevMenuText.text = menuTitles[menuPointer - 1];
            }
           
            subMenu[menuPointer + 1].SetActive(false);
            subMenu[menuPointer].SetActive(true);
            if (menuPointer == 2)
            {
                nextButton.SetActive(true);
            }
            if (menuPointer == 0)
            {
                prevButton.SetActive(false);
            }
        }
    }
    // Update is called once per frame
}
