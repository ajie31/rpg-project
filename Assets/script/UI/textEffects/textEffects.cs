﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class textEffects : MonoBehaviour
{
  //  public Color _color1;
   // public Color _color2;
    [SerializeField]private float lerpVelocity;
    [SerializeField] private RectTransform _transf;
    private bool startFade;
    [SerializeField] private TextMeshProUGUI _text;
    private float timeSinceStarted;
    private float startTimeLerp;
    private float percentage;
    private bool isDamage;
    private Vector3 toUp;

    public void resetStats(bool status, Vector2 post,bool damage)
    {
       toUp = new Vector3(0, .1f, 0);
        _transf.anchoredPosition = Vector2.zero;
        if (!damage)
        {
            if (status)
            {
                _text.text = "Good!";
                _text.color = Color.blue;
            }
            else
            {
                _text.text = "Miss!";
                _text.color = Color.red;
            }
            _transf.anchorMin = post;
            _transf.anchorMax = post;
        }
        else
        {
           // _text.text = playerAttr._pAttribute.getDamage().ToString();
            _text.color = Color.yellow;
            _transf.position = post;
        }

        isDamage = damage;
        startTimeLerp = Time.time;
        timeSinceStarted = 0;
        percentage = 0;
        startFade = true;
    }

    private void FixedUpdate()
    {
        if (startFade)
        {
            timeSinceStarted = Time.time - startTimeLerp;
            percentage = timeSinceStarted / (lerpVelocity / 2);

            _text.alpha = Mathf.Lerp(1, 0, percentage);
            if (isDamage)
            {
                _transf.position += toUp;
            }
           
            if (percentage >= 1f)
            {
                startFade = false;
                gameObject.SetActive(false);
            }
        }
       

    }
}
