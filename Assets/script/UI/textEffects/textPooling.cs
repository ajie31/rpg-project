﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class textPooling : MonoBehaviour
{
    public static textPooling _textPooling;
    [SerializeField] GameObject textGameObject;
    [SerializeField] int textAmount;
    private TextMeshProUGUI[] textMesh;
    [SerializeField] private Transform parent;
    // Start is called before the first frame update

    private void Awake()
    {
        if (_textPooling == null)
        {
            _textPooling = this;
        }
        
    }
    void Start()
    {
        textMesh = new TextMeshProUGUI[textAmount];

        for (int i = 0; i < textAmount; i++)
        {
            GameObject textGo = Instantiate(textGameObject);
            textMesh[i] = textGo.GetComponent<TextMeshProUGUI>();
            textGo.transform.SetParent(parent);
            textGo.transform.localScale = new Vector3(1, 1, 1);
            textGo.GetComponent<RectTransform>().anchoredPosition = Vector3.zero;
            textGo.SetActive(false);
        }
    }

    public TextMeshProUGUI getText(bool status, Vector2 circlePos,bool damage = false)
    {

        for (int i = 0; i < textMesh.Length; i++)
        {
            if (!textMesh[i].gameObject.activeInHierarchy)
            {
                if (status)
                {
                    textMesh[i].GetComponent<textEffects>().resetStats(true,circlePos,damage);
                }
                else
                {
                    textMesh[i].GetComponent<textEffects>().resetStats(false,circlePos,damage);
                }
                return textMesh[i];
            }
           
        }
        return null;
    }



    // Update is called once per frame
  
}
