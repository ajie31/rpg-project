﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class enemyUIManager : MonoBehaviour
{
    Quaternion rotation;
    public Image _HPFill;
    public enemyAtt _enemyAtt;
    private void Awake()
    {
        rotation = transform.rotation;
    }
    private void Start()
    {

        _HPFill.fillAmount = _enemyAtt.ratioHealth();
    }

    private void LateUpdate()
    {
        transform.rotation = rotation;
    }

    public void updateEnemyHealth()
    {

        _HPFill.fillAmount = _enemyAtt.ratioHealth();
    }
 


}
