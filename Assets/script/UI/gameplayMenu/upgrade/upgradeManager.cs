﻿using System.Collections;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class upgradeManager : MonoBehaviour
{
    [Header("Basic Property")]
    public levelingAndPerk _levelPerkObject;
    public gameplayMenu _menu;
    public skillGroup[] _skills;

    #region upgrade pointer property
    private int valueIndexClass;
    private int skillPageIndex;
    private int branchSkill;
    private int upgradeIndex;
    private int upgradeCost;
    #endregion

    [Header("Skill description")]
    public TextMeshProUGUI[] skillDescriptionText;
    public Image[] spriteImages;
    public TextMeshProUGUI[] skillTitle;
    [SerializeField] private TextMeshProUGUI[] skillStatsText;
    [Header("Skill Skill Tree")]
    public gameObjectInside[] skillTrees;
    [Header("Upgrade Pop-Up Confirmation")]
    public GameObject upgradePage;
    public TextMeshProUGUI upgradeMessage;
    [Header("Page Control")]
    public GameObject PrevButtonSkillTree;
    public GameObject NextButtonSkillTree;
    private int valueIndexClassBefore;

    private Button treeSkill;

   

    private void Start()
    {
        valueIndexClass = 0;
        StartCoroutine(setPages(valueIndexClass));
    }

    #region press upgrade trees
    public void upgradeButtonpopUp(int branch, int upgradeVal,bool isConfirm = false)
    {
        if (!isConfirm)
        {
            branchSkill = branch;
            upgradeIndex = upgradeVal;
        }
        if (valueIndexClass == 0)
        {
            if (skillPageIndex == 0)
            {
                ((demonBlood)_skills[valueIndexClass]._skill[skillPageIndex]).upgradeSkill(branch, upgradeIndex, isConfirm);
                upgradeMessage.text = ((demonBlood)_skills[valueIndexClass]._skill[skillPageIndex]).upgradeMessage;
            }
            if (skillPageIndex == 1)
            {
                ((trueStrike)_skills[valueIndexClass]._skill[skillPageIndex]).upgradeSkill(branch, upgradeIndex, isConfirm);
                upgradeMessage.text = ((trueStrike)_skills[valueIndexClass]._skill[skillPageIndex]).upgradeMessage;
            }
            if (skillPageIndex == 2)
            {
                ((dimensionRift)_skills[valueIndexClass]._skill[skillPageIndex]).upgradeSkill(branch, upgradeIndex, isConfirm);
                upgradeMessage.text = ((dimensionRift)_skills[valueIndexClass]._skill[skillPageIndex]).upgradeMessage;
            }
            if (skillPageIndex == 3)
            {
                ((soulRender)_skills[valueIndexClass]._skill[skillPageIndex]).upgradeSkill(branch, upgradeIndex, isConfirm);
                upgradeMessage.text = ((soulRender)_skills[valueIndexClass]._skill[skillPageIndex]).upgradeMessage;
            }
            if (skillPageIndex == 4)
            {
                ((hellWrath)_skills[valueIndexClass]._skill[skillPageIndex]).upgradeSkill(branch, upgradeIndex, isConfirm);
                upgradeMessage.text = ((hellWrath)_skills[valueIndexClass]._skill[skillPageIndex]).upgradeMessage;
            }

        }
        showPopUpUpgrade();
    }
    #endregion
    public IEnumerator setPages(int index)
    {
     
        valueIndexClass = index;
        setSKillStatus();
        for (int i = 0; i < 5; i++)
        {
            skillTitle[i].text = _skills[index]._skill[i].skillName;
            spriteImages[i].sprite = _skills[index]._skill[i].artwork;
            skillDescriptionText[i].text = _skills[index]._skill[i].description;
            skillTrees[index].objects[i].SetActive(true);
            if (valueIndexClass != valueIndexClassBefore)
            {
                skillTrees[valueIndexClassBefore].objects[i].SetActive(false);
            }

            yield return null;
            if (i == 4)
            {
                valueIndexClassBefore = index;
            }
        }

    }

    public void setSKillStatus()
    {
        if (valueIndexClass == 0)
        {
           
            skillStatsText[0].text = ((demonBlood)(_skills[0]._skill[0])).displayDescription();
           
            skillStatsText[1].text = ((trueStrike)(_skills[0]._skill[1])).displayDescription();

            skillStatsText[2].text = ((dimensionRift)(_skills[0]._skill[2])).displayDescription();

            skillStatsText[3].text = ((soulRender)(_skills[0]._skill[3])).displayDescription();

            skillStatsText[4].text = ((hellWrath)(_skills[0]._skill[4])).displayDescription();
        }


    }

    public void setIndexTree(int i)
    {
        skillPageIndex = i;
        if (skillPageIndex == 4)
        {
            NextButtonSkillTree.SetActive(false);
        }
        else
        {
            if (!NextButtonSkillTree.activeInHierarchy)
            {
                NextButtonSkillTree.SetActive(true);
            }
        }

        if (skillPageIndex == 0)
        {
            PrevButtonSkillTree.SetActive(false);
        }
        else
        {
            if (!PrevButtonSkillTree.activeInHierarchy)
            {
                PrevButtonSkillTree.SetActive(true);
            }
        }
    }



    public void confirmUpgrade()
    {
        upgradeButtonpopUp(branchSkill, upgradeIndex, true);
        upgradePage.SetActive(false);
        if (treeSkill != null)
        {
            treeSkill.interactable = false;
            _levelPerkObject.decreaseSkillPoint(upgradeCost);
            _menu.setSPText();
        }
        else
        {
            Debug.Log("no button set");
        }
    }
    public void setButtonSkillUp(Button _btt)
    {
        treeSkill = _btt;
    }
    public void setCostUpgrade(int val)
    {
        upgradeCost = val;
    }

    public void showPopUpUpgrade()
    {
        upgradePage.SetActive(true);
    }
    public void cancelUpgradeButton()
    {
        upgradePage.SetActive(false);
    }
}
