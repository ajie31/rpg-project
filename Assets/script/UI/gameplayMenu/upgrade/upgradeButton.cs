﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class upgradeButton : MonoBehaviour
{
    public upgradeManager _upgradeM;
    public bool isUpgrade;
    public Button confirmUpgrade;
    public buttonCustom _custBtt;
    public TextMeshProUGUI costText;
    public TextMeshProUGUI perkLevel;
    public TextMeshProUGUI upgradeReq;
    private Button _button;
   // private bool onceSP;
    char indexC, indexC1;
    int index = 0, index1 = 0;
    string indexS;

    private void Start()
    {
        if (isUpgrade)
        {
            indexS = gameObject.name;
            indexC = indexS[0];
            indexC1 = indexS[1];

            int.TryParse(indexC.ToString(), out index);
            int.TryParse(indexC1.ToString(), out index1);
        }
        _button = GetComponent<Button>();
        _button.onClick.AddListener(buttonSkillUpgrade);
        

        _custBtt.checkButtonStats(_button);
    }



    public void buttonSkillUpgrade()
    {
        _upgradeM.setCostUpgrade(_custBtt.skillPointCost);
       // onceSP = false;
        int reqUppgradeNumber = PlayerPrefs.GetInt(_custBtt.idUpgradeReq);
        _upgradeM.upgradeButtonpopUp(index, index1);
        _upgradeM.setButtonSkillUp(_button);
        costText.text = "[" + _custBtt.skillPointCost.ToString() + "]";
        perkLevel.text = "Perk Level level : " + _custBtt.perkLevelReq.ToString() + "*";
        upgradeReq.text = _custBtt.upgradeRequirement + " : " + _custBtt.upgradeReqTier;
        if (_upgradeM._levelPerkObject._pStatus.playerSkillPt < _custBtt.skillPointCost || 
            (_upgradeM._levelPerkObject._pStatus._playerPerk[(int)_upgradeM._levelPerkObject._pStatus._weaponClass]._perkLevel < _custBtt.perkLevelReq ||
            reqUppgradeNumber < _custBtt.upgradeReqTier))
        {
            if (_upgradeM._levelPerkObject._pStatus._playerPerk[(int)_upgradeM._levelPerkObject._pStatus._weaponClass]._perkLevel < _custBtt.perkLevelReq)
            {
                perkLevel.color = Color.yellow;
            }
            else
            {
                perkLevel.color = Color.green;
            }

            if (reqUppgradeNumber < _custBtt.upgradeReqTier)
            {
                upgradeReq.color = Color.yellow;
            }
            else
            {
                upgradeReq.color = Color.green;
            }
            confirmUpgrade.interactable = false;
        }
        else
        {
            perkLevel.color = Color.green;
            upgradeReq.color = Color.green;
            confirmUpgrade.interactable = true;
        }
    }


    
}
