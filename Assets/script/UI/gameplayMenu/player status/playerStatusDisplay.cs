﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class playerStatusDisplay : MonoBehaviour
{
    [Header("playerAttr attribute")]
    #region Player Attribute
    public TextMeshProUGUI[] _attributesText;
    public playerStatusOBJ _pStatus;
    public playerWeaponManager _pWeaponM;
    #endregion

    [Header("Player Bars HP & Mana")]
    #region Bars HP Mana
    public Image healthPointFill;
    public Image manaFill;
   
    #endregion
    [Header("XP property")]
    #region XP
    public Image ExpProgress;
    public TextMeshProUGUI xpText;
    #endregion

    [Header("player Perks")]
    #region player Perks
    public TextMeshProUGUI[] perkLevelText;
    public Image[] perkFillProgress;
    #endregion

    [Header("Player Weapon Indicator")]
    public Sprite[] WeaponIcon;
    public Image[] weaponClassImage;
    public Image[] weaponClassBG;
    public Color[] classColor;

    private void setupStatusText()
    {
        for (int i = 0; i < _pStatus._attributes.Length; i++)
        {
            
            if (i==0 || i == 6)
            {
                _attributesText[i].text = _pStatus._attributes[i].actualValue + " / " + _pStatus._attributes[i].totalValue;
                if (i == 0)
                {
                    healthPointFill.fillAmount = setupHealthMana(i);
                }
                else
                {
                    manaFill.fillAmount = setupHealthMana(i);
                }
            }
            else
            {
                _attributesText[i].text = _pStatus._attributes[i].totalValue.ToString("0.0");
            }
        }
    }

    private float perkPercent(int i)
    {
        return _pStatus._playerPerk[i]._PerkProgress / _pStatus._playerPerk[i]._maxPerk;
        
    }

    private void perkSetup()
    {
        for (int j = 0; j < _pStatus._playerPerk.Length; j++)
        {
            perkLevelText[j].text = _pStatus._playerPerk[j]._perkLevel.ToString();
            perkFillProgress[j].fillAmount = perkPercent(j);
        }
    }
    private float setupHealthMana(int i)
    {
        return  _pStatus._attributes[i].actualValue /  _pStatus._attributes[i].totalValue;      
    }

    private void weaponClassSetup()
    {
        for (int i = 0; i < weaponClassImage.Length; i++)
        {
            weaponClassImage[i].sprite = WeaponIcon[(int)(_pWeaponM._weaponChosen[i]._weaponClass)];
            weaponClassBG[i].color = classColor[(int)(_pWeaponM._weaponChosen[i]._weaponClass)];
        }
    }
    private void XpSetup()
    {
        ExpProgress.fillAmount = _pStatus.getExpPercent();
        xpText.text = _pStatus.playerExp + " / " + _pStatus.maxXp.ToString(); 
    }

    private void OnEnable()
    {
        setupStatusText();
        perkSetup();
        XpSetup();
        weaponClassSetup();
    }

}
