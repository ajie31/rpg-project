﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
//using TMPro;
public class inventoryDisplay : MonoBehaviour
{
    [SerializeField] private inventoryObject[] playerInventory;
    [Header("General")]
    #region property inventory slot
    public Transform[] inventorySlot;
    public itemType type;
    private Transform child0, child1;
    private int indexEquiped;
    #endregion
    [Header("Item Description")]
    #region property item description
    public TextMeshProUGUI itemName;
    public Image itemArtwork;
    public GameObject[] itemAttributes;
    public Sprite[] attributeSprites;
    public Image[] attributeIcons;
    public TextMeshProUGUI[] itemAttText;
    public TextMeshProUGUI[] itemAttValText;
    public TextMeshProUGUI itemDescription;
    public ToggleGroup _toggleGroup;
    private inventorySlot selectedItem;
    private GameObject equipedBG,equipedNewBG;
    #endregion
    [Header("Player Attribute")]
    #region property of player attributes
    public playerStatusOBJ _pStatus;
    public Transform[] playerAttributes;
    private inventorySlot itemEquiped;
    public Color playerStatusInitial;
    #endregion

    [Header("Button Equip/Unequip")]
    #region button equip/unequip
    public Image buttonEUImage;
    public TextMeshProUGUI buttonEquipText;
    public Color buttonEquipCollor;
    #endregion

    #region equip weapon
    public playerWeaponManager _pWeaponM;

    #endregion
    [Header("Choose Equipment")]
    #region choose equipment
    public Color selectedEquipment;
    public Color sNormalEquipment;
    private Image equipmentTypeImage;
    #endregion

    #region selecting item
    public void discardItem(Transform slot)
    {
        int slotIndex = 0;
        slotIndex = slot.GetSiblingIndex();

        playerInventory[(int)type].container.RemoveAt(slotIndex);
        resetDisplay();
        createDisplay((int)type);

    }

    public void equipUnequipDynamicSetup()
    {
        if (itemEquiped != null)
        {
            if (selectedItem == itemEquiped)
            {
                buttonEquipText.text = "Unequip";
                buttonEUImage.color = buttonEquipCollor;
            }
            else
            {
                buttonEquipText.text = "Equip";
                buttonEUImage.color = Color.white;
            }
        }
        else
        {
            buttonEquipText.text = "Equip";
            buttonEUImage.color = Color.white;
        }

    }

    public void equipUnequip()
    {
        if (itemEquiped != null)
        {
            if (selectedItem == itemEquiped)
            {
                unEquipItem();
                equipUnequipDynamicSetup();
            }
            else
            {
                equipItem();
                equipUnequipDynamicSetup();
            }
        }
        else
        {
            equipItem();
            equipUnequipDynamicSetup();
        }
    }

    public void equipItem()
    {
        if (selectedItem != null)
        {
            if (itemEquiped == null)
            {
                selectedItem.isEquiped = true;
                itemEquiped = selectedItem;

                if (type == itemType.weapon)
                {
                    _pWeaponM._weaponChosen[indexEquiped] = (weaponObject)selectedItem.item;
                }
                else
                {
                    foreach (var item in selectedItem.item._attBonus)
                    {
                        _pStatus.getAttribute[item.attribute].bonusValue += item.bonusAtt;
                        _pStatus.resetTotalValue(item.attribute);
                    }
                }

            }
        }
        else
        {
            itemEquiped.isEquiped = false;
            selectedItem.isEquiped = true;
            if (type == itemType.weapon)
            {
                _pWeaponM._weaponChosen[indexEquiped] = (weaponObject)selectedItem.item;
            }
            else
            {
                foreach (var item in itemEquiped.item._attBonus)
                {
                    _pStatus.getAttribute[item.attribute].bonusValue -= item.bonusAtt;

                }
                foreach (var item in selectedItem.item._attBonus)
                {
                    _pStatus.getAttribute[item.attribute].bonusValue += item.bonusAtt;
                    _pStatus.resetTotalValue(item.attribute);
                }
            }


            itemEquiped = selectedItem;
            equipedBG.SetActive(false);
        }

        _pStatus.resetIsCompared();
        resetplayerAttributeDisplay(true);
        showPlayerStatus();

        equipedNewBG.SetActive(true);
        equipedBG = equipedNewBG;

    }

    public void unEquipItem()
    {
        equipedBG.SetActive(false);
        itemEquiped.isEquiped = false;
        foreach (var attrib in itemEquiped.item._attBonus)
        {

            _pStatus.getAttribute[attrib.attribute].bonusValue -= attrib.bonusAtt;
            _pStatus.getAttribute[attrib.attribute].isCompare = false;
        }
        _pStatus.resetIsCompared();
        resetplayerAttributeDisplay();
        showPlayerStatus();
        itemEquiped = null;
    }
    #endregion

    #region show description

    public void showItemDescription(Transform selected)
    {
        clearItemDescription();
        
        if (_toggleGroup.allowSwitchOff)
        {
            _toggleGroup.allowSwitchOff = false;
        }
        if ( selected.GetChild(0).GetComponent<Toggle>().isOn)
        {
            
            int i = 0;            
            i = selected.GetSiblingIndex();
            child1 = selected.GetChild(1);
            selectedItem = playerInventory[(int)type].container[i];
            calculateComparisonVal();
           
            equipUnequipDynamicSetup();
            /**
             * item selected here
             */
           
            equipedNewBG = selected.GetChild(0).GetChild(0).GetChild(1).gameObject;
            // Debug.Log(i);
            itemName.text = selectedItem.item.itemName;
            itemDescription.text = selectedItem.item.description;
            itemArtwork.gameObject.SetActive(true);
            itemArtwork.sprite = selectedItem.item._sprite;
            child1.GetComponent<Button>().interactable = true;
            for (int j = 0; j < selectedItem.item._attBonus.Length; j++)
            {
                itemAttributes[j].SetActive(true);
                itemAttText[j].text = selectedItem.item._attBonus[j].attributeName;
                itemAttValText[j].text = selectedItem.item._attBonus[j].bonusAtt.ToString();
                
                attributeIcons[j].sprite = attributeSprites[(int)(selectedItem.item._attBonus[j].attribute)];
            }

            /**
                 * add player status dispay 
                 * **/
            showPlayerStatus();
        }
    }
    /**
     *clear item description has reset compared value and reset status attribute display 
     *
     *
     */
    public void clearItemDescription()
    {
        _pStatus.resetIsCompared();
        resetplayerAttributeDisplay();
        itemName.text = null;
        itemArtwork.gameObject.SetActive(false);
        itemDescription.text = null;
        selectedItem = null;
        if (child1 != null)
        {
            child1.GetComponent<Button>().interactable = false;
        }
       
        foreach (var attr in itemAttributes)
        {
            attr.SetActive(false);
        }
    }

    #endregion
    #region display Inventory 

    public void resetDisplay()
    {
        if (equipmentTypeImage != null)
        {
            equipmentTypeImage.color = sNormalEquipment;
            equipmentTypeImage.transform.GetChild(0).GetComponent<Image>().color = selectedEquipment;
        }      
        _toggleGroup.allowSwitchOff = true;
        itemEquiped = null;
        if (child1 != null)
        {
            child1.GetComponent<Button>().interactable = false;
            child1 = null;
        }        
        foreach (var slot in inventorySlot)
        {
            child0 = slot.GetChild(0);
            //child1 = slot.GetChild(1);
            
            if (child0.GetComponent<Toggle>().interactable)
            {
                child0.GetChild(0).GetChild(2).GetComponent<Image>().sprite = null; // sprite here
                child0.GetComponent<Toggle>().interactable = false;
                child0.GetComponent<Toggle>().isOn = false;
                child0.GetChild(0).GetChild(2).gameObject.SetActive(false);
                child0.GetChild(0).GetChild(1).gameObject.SetActive(false);
                // child1.GetComponent<Button>().interactable = false;
            }
        }
    }


    public void createDisplay(int index)
    {
       
        for (int i = 0; i < playerInventory[index].container.Count; i++)
        {            
            child0 = inventorySlot[i].GetChild(0);

            child0.GetChild(0).GetChild(2).GetComponent<Image>().sprite = playerInventory[index].container[i].item._sprite; // sprite here           
            child0.GetChild(0).GetChild(2).gameObject.SetActive(true);

            if ((type == itemType.weapon || type == itemType.ring || type == itemType.earRing)
                && playerInventory[index].container[i].isEquiped )
            {
                if (indexEquiped == playerInventory[index].container[i].indexEquip)
                {
                    child0.GetComponent<Toggle>().interactable = true;

                    child0.GetComponent<Toggle>().isOn = true;
                    itemEquiped = playerInventory[index].container[i];
                    showItemDescription(inventorySlot[i]);
                    equipedNewBG = child0.GetChild(0).GetChild(1).gameObject;
                    equipedNewBG.SetActive(true);
                    equipedBG = child0.GetChild(0).GetChild(1).gameObject;
                }
                else
                {
                    child0.GetComponent<Toggle>().interactable = false;
                }

            }
            else
            {
                child0.GetComponent<Toggle>().interactable = true;
                if (playerInventory[index].container[i].isEquiped)
                {
                    child0.GetComponent<Toggle>().isOn = true;
                    itemEquiped = playerInventory[index].container[i];
                    showItemDescription(inventorySlot[i]);
                    equipedNewBG = child0.GetChild(0).GetChild(1).gameObject;
                    equipedNewBG.SetActive(true);
                    equipedBG = child0.GetChild(0).GetChild(1).gameObject;

                }
            }
            
            // child1.GetComponent<Button>().interactable = true;       
        }
    }

    #region showcase 
        public void showMasks(Image _image)
        {          
            indexEquiped = -1;
            type = itemType.mask;
            resetDisplay();
            createDisplay((int)type);
            _image.color = selectedEquipment;
            _image.transform.GetChild(0).GetComponent<Image>().color = sNormalEquipment;
             equipmentTypeImage = _image;
         }
        public void showEarRings1(Image _image)
        {          
            indexEquiped = 1;   
            type = itemType.earRing;
            resetDisplay();
            createDisplay((int)type);
            _image.color = selectedEquipment;
            _image.transform.GetChild(0).GetComponent<Image>().color = sNormalEquipment;
            equipmentTypeImage = _image;
        }
        public void showEarRings2(Image _image)
        {          
            indexEquiped = 1;
            type = itemType.earRing;
            resetDisplay();
            createDisplay((int)type);
            _image.color = selectedEquipment;
            _image.transform.GetChild(0).GetComponent<Image>().color = sNormalEquipment;
            equipmentTypeImage = _image;
        }
        public void showGloves(Image _image)
        {          
            indexEquiped = -1;
            type = itemType.glove;
            resetDisplay();
            createDisplay((int)type);
            _image.color = selectedEquipment;
            _image.transform.GetChild(0).GetComponent<Image>().color = sNormalEquipment;
            equipmentTypeImage = _image;
        }
        public void showWeapons1(Image _image)
        {
            _image.color = selectedEquipment;
            _image.transform.GetChild(0).GetComponent<Image>().color = sNormalEquipment;
            indexEquiped = 0;
            type = itemType.weapon;
            resetDisplay();
            createDisplay((int)type);
            equipmentTypeImage = _image;
        }
        public void showWeapons2(Image _image)
        {
            indexEquiped = 1;
            type = itemType.weapon;
            resetDisplay();
            createDisplay((int)type);
            _image.color = selectedEquipment;
            _image.transform.GetChild(0).GetComponent<Image>().color = sNormalEquipment;
            equipmentTypeImage = _image;
        }
        public void showBodyArmor(Image _image)
        {          
            indexEquiped = -1;
            type = itemType.bodyArmor;
            resetDisplay();
            createDisplay((int)type);
            _image.color = selectedEquipment;
            _image.transform.GetChild(0).GetComponent<Image>().color = sNormalEquipment;
            equipmentTypeImage = _image;
        }
        public void showRings1(Image _image)
        {          
            indexEquiped = 0;
            type = itemType.ring;
            resetDisplay();
            createDisplay((int)type);
            _image.color = selectedEquipment;
            _image.transform.GetChild(0).GetComponent<Image>().color = sNormalEquipment;
            equipmentTypeImage = _image;
        } 
        public void showRings2(Image _image)
        {         
            indexEquiped = 1;
            type = itemType.ring;
            resetDisplay();
            createDisplay((int)type);
            _image.color = selectedEquipment;
            _image.transform.GetChild(0).GetComponent<Image>().color = sNormalEquipment;
            equipmentTypeImage = _image;
        }
        public void showBoots(Image _image)
        {          
            indexEquiped = -1;
            type = itemType.boot;
            resetDisplay();
            createDisplay((int)type);
            _image.color = selectedEquipment;
            _image.transform.GetChild(0).GetComponent<Image>().color = sNormalEquipment;
            equipmentTypeImage = _image;
        }
    #endregion

    #endregion

    #region show player Status

    private void calculateComparisonVal()
    {
        if (itemEquiped != null)
        {
            foreach (var attrib in itemEquiped.item._attBonus)
            {
                
                _pStatus.editComparisonValue(_pStatus.getAttribute[attrib.attribute], -attrib.bonusAtt);
                _pStatus.getAttribute[attrib.attribute].isCompare = true;
            }
            foreach (var item in selectedItem.item._attBonus)
            {
                _pStatus.editComparisonValue(_pStatus.getAttribute[item.attribute], item.bonusAtt);
                _pStatus.getAttribute[item.attribute].isCompare = true;
            }
        }
        else
        {
            foreach (var item in selectedItem.item._attBonus)
            {
                _pStatus.editComparisonValue(_pStatus.getAttribute[item.attribute], item.bonusAtt);
                _pStatus.getAttribute[item.attribute].isCompare = true;
            }
        }        
    }

    private void showPlayerStatus()
    {
        
        int attribIndex = -1;
        foreach (var attrib in _pStatus._attributes)
        {
            if (attrib.isCompare)
            {
                attribIndex++;
                playerAttributes[attribIndex].gameObject.SetActive(true);
                playerAttributes[attribIndex].GetChild(0).GetComponent<Image>().sprite = attributeSprites[(int)attrib._attr];
                playerAttributes[attribIndex].GetChild(1).GetComponent<TextMeshProUGUI>().text = attrib.name;
                playerAttributes[attribIndex].GetChild(2).GetComponent<TextMeshProUGUI>().text = (attrib.valueComparison + attrib.totalValue).ToString("0");
                if (attrib.valueComparison + attrib.totalValue > attrib.totalValue)
                {
                    playerAttributes[attribIndex].GetChild(2).GetComponent<TextMeshProUGUI>().color = Color.green;
                }
                else if (attrib.valueComparison + attrib.totalValue < attrib.totalValue)
                {
                    playerAttributes[attribIndex].GetChild(2).GetComponent<TextMeshProUGUI>().color = Color.red;
                }
               
            }
            
        }
    }

    private void resetplayerAttributeDisplay(bool isColor = false)
    {
        foreach (var attrib in playerAttributes)
        {
            if (attrib.gameObject.activeInHierarchy)
            {
                if (!isColor)
                {
                    attrib.gameObject.SetActive(false);
                }
                
                attrib.GetChild(2).GetComponent<TextMeshProUGUI>().color = playerStatusInitial;
            }
            
        }
    }

    #endregion
}
