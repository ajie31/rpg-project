﻿using UnityEngine;

public class paginationSkills : MonoBehaviour
{
    public upgradeManager _upgradeM;

    public  void pageActive(bool status)
    {
        if (status)
        {
            StartCoroutine(_upgradeM.setPages(transform.GetSiblingIndex()));
        }
    }

    public void contentActive(bool status)
    {
        if (status)
        {
            _upgradeM.setIndexTree(transform.GetSiblingIndex());         
        }
    }
}
