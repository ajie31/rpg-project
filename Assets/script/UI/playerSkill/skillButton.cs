﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class skillButton : MonoBehaviour
{
    private float cooldown;
    private float Count;
    private bool isReady = true;
    private Image bttnImage;
    private void Start()
    {
        bttnImage = GetComponent<Image>();
    }

    private void Update()
    {
        if (Count <  cooldown && !isReady)
        {
           // Debug.Log(Count);
            bttnImage.fillAmount = Count/cooldown;
            //bttnText.text = ((int)cooldown).ToString();
            Count += Time.deltaTime; 
        }
        else if (Count >= cooldown && !isReady)
        {
            bttnImage.fillAmount = 1;
            cooldown = 0;
            Count = 0;
            isReady = true;
          //  bttnText.text = (transform.GetSiblingIndex()).ToString();
        }
    }

    public void skillBtt()
    {
         int index = 0;
         int.TryParse(gameObject.name, out index);
        if (isReady && cooldown <= 0)
        {

            skillMan._skillsManager.getSkills(index);//transform.parent.GetSiblingIndex() - 1);
           // playerUIManager._pUIManager.updateManaVal();
            isReady = false;
            Count = 0;
            cooldown = skillMan._skillsManager.getCooldown();//transform.parent.GetSiblingIndex() - 1);
        }
   
       
    }

   
}
