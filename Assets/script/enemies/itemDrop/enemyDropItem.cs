﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyDropItem : MonoBehaviour
{
    public GameObject item;
    public enemyDrops _drop;


    private void OnEnable()
    {
        item.transform.SetParent(transform);
        item.transform.localPosition = Vector2.zero;
        item.SetActive(false);
    }
}
