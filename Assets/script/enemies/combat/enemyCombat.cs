﻿
using SAP2D;
using UnityEngine;

public class enemyCombat : MonoBehaviour
{
    public SAP2DAgent _agentE;
    public enemyAtt _eAttr;
    public float delayAttack = 0.7f; 
    float delayAttackCount;

    public LayerMask playerLayer;

    private void Start()
    {
        delayAttackCount = delayAttack;
    }
    private void Update()
    {
        if ( _agentE._State == State.attacking)
        {
            /* insert animation */
            if (delayAttackCount <= 0)
            {
               
                delayAttackCount = delayAttack;
                attacking();

            }
            else
            {
                delayAttackCount -= Time.deltaTime;
            }
        }

    }

    public void attacking()
    {
        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(transform.position, _eAttr._profile.weaponRange, playerLayer);

        foreach (var enemy in hitEnemies)
        {
            _eAttr.damageOutPut();              
            if (_eAttr._profile._pStatusEffet.getReflectDamage())
            {
                  _eAttr.damageInputPure(_eAttr._profile._pDamageIO.damageTaken);
            }
        }
    }



}
