﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SAP2D;
//using Polarith.AI.Move;

public class enemyCollider : MonoBehaviour
{

    public enemyAtt _eAttr;
    public enemyUIManager _enemyUI;
    private Transform playerT;

   // private Vector2 knockPost;
    WaitForSeconds _waitSec;
    private enemyMove _eMove;
    private SAP2DAgent _agentE;

    private void Start()
    {
        _eMove = GetComponent<enemyMove>();
        _waitSec = new WaitForSeconds(1f);   
        _agentE = GetComponent<SAP2DAgent>();    
    }

    private void Update()
    {
        #region Detect player
        if (Vector2.Distance(transform.position, _eAttr.getPlayerTransform().position) <= _eAttr._profile.rangeDetection && !_eMove.detecting)
        {
            if (_eAttr.getPlayerTransform().gameObject.activeInHierarchy)
            {
                _eMove.detecting = true;
                _agentE._State = State.moving;
                playerT = _eAttr.getPlayerTransform();
            }
        }
        /** if player run away*/
        else if ((Vector2.Distance(transform.position, _eAttr.getPlayerTransform().position) > _eAttr._profile.rangeDetection 
            || !_eAttr.getPlayerTransform().gameObject.activeInHierarchy) && _eMove.detecting)
        {
            
            _eMove.detecting = false;
            _agentE._State = State.moving;

        }
        #endregion

        #region attacking
        if (Vector2.Distance(transform.position, _eAttr.getPlayerTransform().position) <= _eAttr._profile.weaponRange && 
            _agentE._State != State.attacking && _agentE._State != State.stop)
        {
            
            _agentE._State = State.attacking;
            _agentE.MovementSpeed = 0;
        }
        /** out of range to attack */
        if (Vector2.Distance(transform.position, _eAttr.getPlayerTransform().position) > _eAttr._profile.weaponRange &&
            _agentE._State == State.attacking )
        {
            _agentE._State = State.moving;
            _agentE.MovementSpeed = _eAttr.movementSpeed;
        }
        #endregion
    }

    private IEnumerator resetAgent()
    {
        yield return _waitSec;
        _agentE.CanSearch = true;
    }
    
    public void resetStatsCollider()
    {
        _enemyUI.updateEnemyHealth();
        _eMove.detecting = false;
        _agentE.MovementSpeed = _eAttr.movementSpeed;
        StartCoroutine(resetAgent());
    }
    
    public Transform getPlayer()
    {

        return playerT;
    }
   
}
