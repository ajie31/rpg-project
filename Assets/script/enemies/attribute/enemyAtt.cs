﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using SAP2D;

public class enemyAtt : MonoBehaviour
{
    public enemyProfile _profile;
    [SerializeField] private enemyUIManager _uiEnemy;
    [SerializeField] private SpriteRenderer _render;
    [SerializeField] private GameObject _healthBar;
    [SerializeField] private SAP2DAgent _agent;
    [SerializeField] private Transform backPost;
    [SerializeField] private Transform frontPos;
    
    [SerializeField] private enemyDropItem _drop;
    private Rigidbody2D rb;

    private Vector2 position;

    private TextMeshProUGUI _text;
    private float damageProcess;

    private Transform playerTrans;

    //private bool gotStun;
    public float healthPoint;
    public float attack;
    public float armor;
    public float movementSpeed;

    private float stunTimer;

    private bool gotKnocked;
    private float knockTime;

    private bool slowDebuff;
    private float slowDebufffTime;

    private bool gotSilent;
    private float silentDuration;


    private void OnEnable()
    {
        _profile.enemyRegister(transform, _uiEnemy);
    }

    private void Start()
    {
        playerTrans = GameObject.FindGameObjectWithTag("Player").transform;
       
        rb = GetComponent<Rigidbody2D>();
    }
    private void Update()
    {
        #region stun enemy
        if (stunTimer > 0)
        {
            if (_agent._State != State.stop)
            {
                if (!gotKnocked)
                {
                    rb.constraints = RigidbodyConstraints2D.FreezePosition;
                }
                _agent._State = State.stop;

            }
            stunTimer -= Time.deltaTime;
        }
        else if (_agent._State == State.stop && stunTimer <= 0)
        {
            stunTimer = 0;
            _agent._State = State.moving;
            _agent.MovementSpeed = movementSpeed;
        }
        #endregion


        #region knock enemy
        if (knockTime > 0)
        {
            knockTime -= Time.deltaTime;
        }
        else if (knockTime <= 0 && gotKnocked)
        {
            rb.velocity = Vector2.zero;
            gotKnocked = false;

            rb.constraints = RigidbodyConstraints2D.None;
        }

        #endregion

        #region slow enemy
        if (slowDebufffTime > 0)
        {
            slowDebufffTime -= Time.deltaTime;
        }
        else if (slowDebuff && slowDebufffTime <= 0)
        {
            slowDebuff = false;
            movementSpeed = _profile.getTotalAttributeValue(attributes.MovementSpd);
            _agent.MovementSpeed = movementSpeed;
            slowDebufffTime = 0;
        }

        #endregion

        #region silent Enemy
        if (silentDuration > 0)
        {
            silentDuration -= Time.deltaTime;
        }
        else if (silentDuration <= 0 && gotSilent)
        {
            gotSilent = false;
            silentDuration = 0;
        }
        #endregion

    }

    #region enemy position and transform
    public void setPosition(Vector2 pos)
    {
        position = pos;
    }
    public Vector2 getPosition()
    {
        return position;
    }

    public Transform getBackPost()
    {

        return backPost;
    }
    public Transform getFrontPos()
    {

        return frontPos;
    }
    #endregion

    public void setLifeStats(bool stats)
    {
        if (stats)
        {
            _drop._drop.generateItem(_drop.item);
            gameObject.SetActive(false);
            _agent.CanSearch = false;
        }
        _profile.deaths = stats;
    }

    public float ratioHealth()
    {
        return healthPoint / _profile.getTotalAttributeValue(attributes.Health);
    }

    #region damage Out
    public void damageOutPut()
    {
        _profile._pDamageIO.damageInput(attack);
    }
    #endregion
    #region damage Input
    public void damageInput(float attack)
    {

        damageProcess = (attack * (100 / (100 + armor)));
        healthPoint -= damageProcess;
        _profile._pDamageIO.damageRecieveFromEnemy(damageProcess);
        _text = textPooling._textPooling.getText(false, transform.position, true);
        _text.text = damageProcess.ToString("0.00");
        _text.gameObject.SetActive(true);
        _uiEnemy.updateEnemyHealth();
        //playerAttr._pAttribute.increasePerk();
    }

    public void damageInputPure(float damage)
    {
        healthPoint -= damage;
        _text = textPooling._textPooling.getText(false, transform.position, true);
        _text.text = damage.ToString("0.00");
        _text.gameObject.SetActive(true);
    }
    #endregion


    public void resetStats()
    {
        healthPoint = _profile.getTotalAttributeValue(attributes.Health);
        attack = _profile.getTotalAttributeValue(attributes.Attack);
        armor = _profile.getTotalAttributeValue(attributes.Armor);
         movementSpeed = _profile.getTotalAttributeValue(attributes.MovementSpd);
        _agent.MovementSpeed = movementSpeed;
        _profile.deaths = false;
        _render.material.SetFloat("Vector1_DD7B566E", 0);
        _healthBar.SetActive(false);
        _agent._State = State.idle;
        stunTimer = 0;
        knockTime = 0;
        slowDebufffTime = 0;
    }
   
    public void setTerget(float stats)
    {
        _render.material.SetFloat("Vector1_DD7B566E", stats);

        if (stats<=0)
        {
            _healthBar.SetActive(false);
        }
        else
        {
            _healthBar.SetActive(true);
        }
    }

    public enemyUIManager getUIEnemy()
    {
        return _uiEnemy;
    }

    #region player debuff Stun and Knock
    public void setStunEnemy(float duration)
    {
        stunTimer = duration;
    }
    public void setGotKnocked(bool status)
    {
        gotKnocked = status;
        if (status)
        {
            knockTime = .5f;
        }

    }
    public void activateSlowDebuff(float duration, float speed)
    {
        slowDebuff = true;
        slowDebufffTime = duration;
        movementSpeed = speed;
    }

    public void setSilent(bool status, float duration)
    {
        gotSilent = status;
        silentDuration = duration;
    }

    #endregion

    public Transform getPlayerTransform()
    {
        return playerTrans;
    }

 
}
