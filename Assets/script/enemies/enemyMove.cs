﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SAP2D;
public class enemyMove : MonoBehaviour
{
   [SerializeField] private SAP2DAgent _agentE;

    private enemyAtt _eAtt;
  // private WaitForSeconds _seconds;
   [SerializeField] private Transform targetting;
    private Transform playetTarget;
    public bool detecting;
    private bool refreshing;
    float timer;
    enemyCollider _eColl;
    private Vector2 natPost;
  

    // Start is called before the first frame update
    void Start()
    { 
        _eColl = GetComponent<enemyCollider>();
        _eAtt = GetComponent<enemyAtt>();
        natPost = _eAtt.getPosition();
        targetting.SetParent(null);
    }

    private void Update()
    {
        if (timer <= 4f && !detecting)
        {
            if (refreshing)
            {
               
                this.targetting.position = natPost;
                refreshing = false;
               
            }
            timer += Time.deltaTime;
        }
        if (timer >4 && !detecting && _agentE._State != State.stop)
        {
           
            timer = 0;
            randomMove();

        }

        if (detecting)
        {
            if (!refreshing)
            {
               
                timer = 0;
                playetTarget = _eColl.getPlayer();
                refreshing = true;
            }
            // Debug.Log(_eColl.getPlayer().name);
            targetting.position = playetTarget.position;
           // refreshing = true;
        }

    }

    private void randomMove()
    {
        _agentE.CanSearch = true;
        this.targetting.position = natPost +  new Vector2(Random.Range(0, 2), Random.Range(0, 2));
        _agentE._State = State.moving;
        if (!_agentE.CanSearch)
        {
            _agentE.CanSearch = true;
        }
    }

    //public void setgotStun(bool status)
    //{

    //    _agentE.CanMove = status;
    //    if (!status)
    //    {
    //        _agentE.MovementSpeed = _eAtt.getMovementSpeedEnemy();
    //    }
    //}




}
